-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : Dim 25 juil. 2021 à 19:33
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cinema`
--

-- --------------------------------------------------------

--
-- Structure de la table `calendar`
--

DROP TABLE IF EXISTS `calendar`;
CREATE TABLE IF NOT EXISTS `calendar` (
  `idCalendar` int(20) NOT NULL AUTO_INCREMENT,
  `idRoom` int(30) NOT NULL,
  `hour` varchar(100) NOT NULL,
  `dateFrom` varchar(50) NOT NULL,
  `dateTo` varchar(50) NOT NULL,
  PRIMARY KEY (`idCalendar`),
  KEY `idRoom` (`idRoom`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `calendar`
--

INSERT INTO `calendar` (`idCalendar`, `idRoom`, `hour`, `dateFrom`, `dateTo`) VALUES
(1, 2, '5:00PM ', '2021-07-26', '2021-07-31'),
(2, 4, '10:00PM ', '2021-07-11', '2021-07-17'),
(3, 6, '5:00PM 9:30PM ', '2021-07-25', '2021-07-31'),
(4, 5, '5:00PM ', '2021-08-02', '2021-08-05'),
(5, 1, '5:00PM 8:30PM ', '2021-08-23', '2021-08-31');

-- --------------------------------------------------------

--
-- Structure de la table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `idCustomer` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idCustomer`)
) ENGINE=InnoDB AUTO_INCREMENT=741 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `customer`
--

INSERT INTO `customer` (`idCustomer`) VALUES
(694),
(695),
(696),
(697),
(698),
(699),
(736),
(737),
(738),
(739),
(740);

-- --------------------------------------------------------

--
-- Structure de la table `discount`
--

DROP TABLE IF EXISTS `discount`;
CREATE TABLE IF NOT EXISTS `discount` (
  `idDiscount` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `reduction` double DEFAULT NULL,
  PRIMARY KEY (`idDiscount`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `discount`
--

INSERT INTO `discount` (`idDiscount`, `name`, `type`, `reduction`) VALUES
(1, 'Teenager reduction', 'Teenager', 1.25),
(2, 'Adult reduction', 'Adult', 0.25),
(3, 'Summer Reduction', 'All', 1.4),
(5, 'Child reduction', 'Teenager', 2.5);

-- --------------------------------------------------------

--
-- Structure de la table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `idEmployees` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(20) DEFAULT NULL,
  `lastName` varchar(20) DEFAULT NULL,
  `login` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idEmployees`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `employee`
--

INSERT INTO `employee` (`idEmployees`, `firstName`, `lastName`, `login`, `password`) VALUES
(1, 'Amber', 'James', 'amber.james', 'pass'),
(2, 'Mike', 'Robin', 'mike.robin', '0000'),
(3, 'Amber', 'James', 'amber.james', 'pass'),
(4, 'Mike', 'Robin', 'mike', '0000'),
(5, 'Kim', 'Rojas', 'kim.rojas', 'pwd'),
(6, 'Faith', 'Sholesi', 'faith.sholesi', '1234'),
(7, 'Tom', 'lee', 'tommy', 'leeee'),
(8, 'Kim', 'Rojas', 'kim.rojas', 'pwd'),
(9, 'Faith', 'Sholesi', 'faith.sholesi', '1234'),
(10, 'Tom', 'lee', 'tommy', 'leeee'),
(11, 'Kim', 'Rojas', 'kim.rojas', 'pwd'),
(12, 'Faith', 'Sholesi', 'faith.sholesi', '1234'),
(13, 'Tom', 'lee', 'tommy', 'leeee');

-- --------------------------------------------------------

--
-- Structure de la table `guest`
--

DROP TABLE IF EXISTS `guest`;
CREATE TABLE IF NOT EXISTS `guest` (
  `idCustomer` int(10) NOT NULL,
  PRIMARY KEY (`idCustomer`),
  KEY `idCustomer` (`idCustomer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `guest`
--

INSERT INTO `guest` (`idCustomer`) VALUES
(736),
(737),
(738),
(739),
(740);

-- --------------------------------------------------------

--
-- Structure de la table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE IF NOT EXISTS `member` (
  `idCustomer` int(11) NOT NULL,
  `firstName` varchar(20) DEFAULT NULL,
  `lastName` varchar(20) DEFAULT NULL,
  `login` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idCustomer`),
  KEY `idCustomer` (`idCustomer`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `member`
--

INSERT INTO `member` (`idCustomer`, `firstName`, `lastName`, `login`, `password`, `type`) VALUES
(694, 'Luc', 'Bear', 'luc.bear@gmail.com', '1958', 'Teenager'),
(695, 'Mary', 'Anderson', 'mary', '1234', 'Senior'),
(696, 'John', 'Brown', 'johnyB', 'istme', 'Adult'),
(697, 'Pedro', 'Garcia', 'pedroGarcia', 'myPWD', 'Teenager'),
(698, 'Mia', 'Evans', 'mia.evans', 'iLoveMia', 'Teenager'),
(699, 'Emily', 'Clarke', 'EmIlY', 'GT35k829L', 'Teenager');

-- --------------------------------------------------------

--
-- Structure de la table `movie`
--

DROP TABLE IF EXISTS `movie`;
CREATE TABLE IF NOT EXISTS `movie` (
  `idMovie` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `duration` int(20) NOT NULL,
  `price` double DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `ticketPurchase` int(11) DEFAULT '0',
  `description` varchar(500) DEFAULT NULL,
  `url` varchar(200) NOT NULL,
  PRIMARY KEY (`idMovie`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `movie`
--

INSERT INTO `movie` (`idMovie`, `name`, `duration`, `price`, `status`, `ticketPurchase`, `description`, `url`) VALUES
(1, 'Cast away', 120, 10, 1, 0, 'A FedEx executive undergoes a physical and emotional transformation after crash landing on a deserted island. ... In a life raft, a relatively unharmed Chuck washes up on shore what he will learn is a deserted island, he unaware what has happened to any of his fellow flight mates, or the plane.', 'images/castAway.jpg'),
(2, 'Imitation Game', 300, 15, 1, 0, 'In 1951, two policemen, Nock and Staehl, investigate the mathematician Alan Turing after an apparent break-in at his home. During his interrogation by Nock, Turing tells of his time working at Bletchley Park during the Second World War. ', 'images/imitationGame.jpg'),
(3, 'Le vent se lève', 150, 20, 1, 0, 'The Wind Has Risen (???? – Kaze Tachinu) is a Japanese novel by Hori Tatsuo, written between 1936–37. It is set in a tuberculosis sanitarium in Nagano, Japan. The plot follows a main character identified only with the pronoun \"I\" as he takes care of his fiancée, and then wife, Setsuko, who has been diagnosed with the disease, deciding to stay with her until her death. It was originally serialised in Kaiz?.', 'images/Leventseleve.jpg'),
(4, 'The schindlers list', 142, 4, 1, 0, 'Oskar Schindler is a vain and greedy German businessman who becomes an unlikely humanitarian amid the barbaric German Nazi reign when he feels compelled to turn his factory into a refuge for Jews. Based on the true story of Oskar Schindler who managed to save about 1100 Jews from being gassed at the Auschwitz concentration camp, it is a testament to the good in all of us.', 'images/laListeDeSchindler.jpg'),
(5, 'Million dollar baby', 178, 7, 1, 0, 'In the end, Maggie pleads with Frankie to kill her, and when Frankie says that he cannot make that decision, Maggie bites her tongue off in a suicidal attempt. In the end, Maggie chooses to be euthanized like her dog Alex, and Frankie merely assists Maggie to relieve her of her suffering.', 'images/millionDollarBaby.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `offer`
--

DROP TABLE IF EXISTS `offer`;
CREATE TABLE IF NOT EXISTS `offer` (
  `idCustomer` int(11) NOT NULL,
  `idDiscount` int(11) NOT NULL,
  KEY `idCustomer` (`idCustomer`),
  KEY `idDiscount` (`idDiscount`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `offer`
--

INSERT INTO `offer` (`idCustomer`, `idDiscount`) VALUES
(698, 1),
(695, 3),
(695, 3),
(695, 3);

-- --------------------------------------------------------

--
-- Structure de la table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
CREATE TABLE IF NOT EXISTS `rooms` (
  `idRoom` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `seatAvailable` int(11) DEFAULT NULL,
  `idMovie` int(20) DEFAULT NULL,
  PRIMARY KEY (`idRoom`),
  KEY `idMovie` (`idMovie`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `rooms`
--

INSERT INTO `rooms` (`idRoom`, `name`, `seatAvailable`, `idMovie`) VALUES
(1, 'Universe', 3, 5),
(2, 'Earth', 0, 1),
(3, 'Jupiter', 10, NULL),
(4, 'Saturn', 2, 2),
(5, 'Uranus', 0, 4),
(6, 'Neptune', 9, 3),
(7, 'Mars', 10, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `idTicket` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(50) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `idCalendar` int(11) DEFAULT NULL,
  `idCustomer` int(11) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `nbPlace` int(11) NOT NULL,
  PRIMARY KEY (`idTicket`),
  KEY `idCalendar` (`idCalendar`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ticket`
--

INSERT INTO `ticket` (`idTicket`, `date`, `price`, `idCalendar`, `idCustomer`, `url`, `nbPlace`) VALUES
(1, '2021-07-27 5:00PM', 11.4, 1, 695, 'tickets/1.png', 1),
(2, '2022-07-12 10:00PM', 15, 2, 736, NULL, 1),
(3, '2021-07-27 5:00PM', 10, 1, 737, NULL, 1),
(4, '2022-08-29 5:00PM', 8.4, 5, 695, 'tickets/4.png', 1),
(5, '2022-07-28 5:00PM', 21.4, 3, 695, 'tickets/5.png', 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `guest`
--
ALTER TABLE `guest`
  ADD CONSTRAINT `guest_ibfk_1` FOREIGN KEY (`idCustomer`) REFERENCES `customer` (`idCustomer`);

--
-- Contraintes pour la table `offer`
--
ALTER TABLE `offer`
  ADD CONSTRAINT `idCustomer` FOREIGN KEY (`idCustomer`) REFERENCES `customer` (`idCustomer`),
  ADD CONSTRAINT `idDiscount` FOREIGN KEY (`idDiscount`) REFERENCES `discount` (`idDiscount`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
