package controller;

import java.io.IOException;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.ICustomer;
import model.BD.DAO.CalendarDAO;
import model.BD.DAO.CustomerDAO;
import model.BD.DAO.DiscountDAO;
import model.BD.DAO.EmployeeDAO;
import model.BD.DAO.RoomDAO;
import model.BD.ImpDAO.CalendarImpDAO;
import model.BD.ImpDAO.CustomerImpDAO;
import model.BD.ImpDAO.DiscountImpDAO;
import model.BD.ImpDAO.EmployeeImpDAO;
import model.BD.ImpDAO.RoomImpDAO;
import model.attribut.Calendar;
import model.attribut.Discount;
import model.attribut.Room;
import model.attribut.TypeC.typeC;
import view.HomeController;
import view.customer.IdentifyController;

/**
 * Date: 2021-07-25
 * 
 * @author Faith Sholesi Kim Rojas
 * 
 */

public class Cinema extends Application {
	private static boolean signOut;
	private static ArrayList<Calendar> time;
	private static CustomerApplication customer;
	private static EmployeeApplication employee;
	private static ArrayList<Room> rooms;
	private static ArrayList<Discount> discount;
	private static Stage primaryStage;
	private static DiscountDAO disDAO = new DiscountImpDAO();
	private static RoomDAO roomDAO = new RoomImpDAO();
	private static CustomerDAO<ICustomer> custDAO = new CustomerImpDAO();
	private static EmployeeDAO empDAO = new EmployeeImpDAO();
	private static CalendarDAO calDAO = new CalendarImpDAO();

	public Cinema() throws IllegalArgumentException, IllegalAccessException {

		signOut = false;
		rooms = roomDAO.getRooms();
		discount = disDAO.getDiscounts();
		time = calDAO.getAllCalendar(rooms);
	}

	/**
	 * Displaying all the movies related to the cinema
	 * 
	 * @param rooms list of the rooms
	 * @param hc    controller of the FXML home
	 */
	public static void displayMovie(ArrayList<Room> rooms, HomeController hc) {

		int i = 0;
		boolean isInProgress;

		for (i = 0; i < rooms.size(); i++) {
			if (rooms.get(i) != null) {
				if (rooms.get(i).getMovie().getState()) {
					isInProgress = checkStatusMovie(rooms.get(i).getID());
					if (rooms.get(i).getSeat() == 0) {

						hc.addImage(rooms.get(i).getMovie().getName(), rooms.get(i).getID(), false,
								rooms.get(i).getMovie().getUrl(), isInProgress);
					} else {

						hc.addImage(rooms.get(i).getMovie().getName(), rooms.get(i).getID(), true,
								rooms.get(i).getMovie().getUrl(), isInProgress);
					}
				}
			}
		}
	}

	/**
	 * 
	 * @return the stage of the application
	 */
	public static Stage getPrimaryStage() {
		return primaryStage;
	}

	/**
	 * Knowing if the movie is in progress or stopped
	 * 
	 * @param id id of the movie
	 * @return state of the movie
	 */
	private static boolean checkStatusMovie(int id) {
		ArrayList<Calendar> roomCal = getDateCalendar(id);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d h:mma", new Locale("en"));
		LocalDateTime hourFrom;
		LocalDateTime hourTo;
		LocalDateTime hourNow;
		LocalDate dateFrom;
		LocalDate dateTo;
		LocalDate dateNow;
		String hourFromTab[];

		for (int i = 0; i < roomCal.size(); i++) {
			hourFromTab = roomCal.get(i).getHour().split(" ");
			dateFrom = roomCal.get(i).getDateFrom();
			dateTo = roomCal.get(i).getDateTo();
			dateNow = LocalDate.now();
			for (int j = 0; j < hourFromTab.length; j++) {
				hourFrom = LocalDateTime.parse(LocalDate.now() + " " + hourFromTab[j], formatter);

				hourTo = hourFrom.plusMinutes(roomCal.get(i).getRoom().getMovie().getDuration());

				hourNow = LocalDateTime.now();


				if (((dateNow.isAfter(dateFrom) || dateNow.isEqual(dateFrom))
						&& (dateNow.isBefore(dateTo) || dateNow.isEqual(dateTo)))
						&& ((hourNow.isAfter(hourFrom) || (hourNow.isEqual(hourFrom))) && hourNow.isBefore(hourTo)
								|| (hourNow.isEqual(hourTo)))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Having the list of the hours for a movie related to a room
	 * 
	 * @param idRoom id of the room
	 * @return the list of the hours
	 */
	public static ArrayList<Calendar> getDateCalendar(int idRoom) {
		ArrayList<Calendar> roomCal = new ArrayList<Calendar>();
		for (Calendar cal : time) {
			if (cal.getRoom().getID() == idRoom) {

				roomCal.add(cal);
			}
		}
		return roomCal;
	}

	@Override
	public void start(Stage primaryS) throws Exception {
		try {

			FXMLLoader load = new FXMLLoader(getClass().getResource("/view/home.fxml"));
			primaryStage = primaryS;

			BorderPane root = (BorderPane) load.load();
			HomeController hc = load.getController();
			displayMovie(rooms, hc);

			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("customer.css").toExternalForm());
			scene.setFill(Color.TRANSPARENT);

			primaryStage.setScene(scene);
			primaryStage.initStyle(StageStyle.TRANSPARENT);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Knowing the type of customer or employee
	 * 
	 * @param login    login of the person
	 * @param password password of the person
	 * @param ic       controller of the FXML identify
	 */
	public static void autentification(String login, String password, IdentifyController ic) {

		if (!login.isEmpty() || !password.isEmpty()) {
			if (custDAO.getCustomer(login, password) != null) {

				if (signOut) {
					customer.setCustomer(getDiscountAccessible((custDAO.getCustomer(login, password)).getType()),
							custDAO.getCustomer(login, password));
				} else {
					signOut = true;
					customer = new CustomerApplication(
							getDiscountAccessible((custDAO.getCustomer(login, password)).getType()), rooms, time,
							custDAO.getCustomer(login, password));
				}
				CustomerApplication.displayHomeMember();
			}

			if (empDAO.getEmployee(login, password) != null) {

				employee = new EmployeeApplication(discount, empDAO.getEmployee(login, password));
				EmployeeApplication.displayHomeEmployee();

			}
		}
		ic.displayError();

	}

	/**
	 * Getting the discounts that the customer can choose (depending of their type)
	 * 
	 * @param type the type of the customer
	 * @return the discounts accessible by the customer
	 */
	private static ArrayList<Discount> getDiscountAccessible(typeC type) {
		ArrayList<Discount> disc = new ArrayList<Discount>();
		for (Discount dis : discount)
			if (dis.getType() == typeC.All || dis.getType() == type) {
				disc.add(dis);
			}
		return disc;
	}

	/**
	 * Displaying the window for buying movies
	 * 
	 * @param imageView the image linked to the movie chosen
	 */
	public static void displayBuyMovie(ImageView imageView) {
		if (!signOut) {
			customer = new CustomerApplication(discount, rooms);
			CustomerApplication.createGuest();
		}
		customer.displayBuyMovie(imageView);
	}

	/**
	 * To change the scene of the application
	 * 
	 * @param url          url of the FXML
	 * @param primaryStage stage of the application
	 * @param visible      knowing if the scene is visible or not
	 * @param css          the css related to the scene
	 * @param style        knowing if we have to set a ID to apply a stylesheet
	 * @return the FXMLLoader related to a window
	 */
	public static FXMLLoader changeScene(String url, Stage primaryStage, boolean visible, String css, boolean style) {
		try {
			FXMLLoader load = new FXMLLoader(Cinema.class.getResource(url));
			Pane root = (Pane) load.load();
			if (style) {
				root.setId("pane");
			}
			Scene scene = new Scene(root);
			scene.getStylesheets().add(Cinema.class.getResource(css).toExternalForm());
			Platform.runLater(new Runnable() {
				public void run() {
					if (!visible) {
						scene.setFill(Color.TRANSPARENT);
					}
					primaryStage.setScene(scene);
					primaryStage.centerOnScreen();
					primaryStage.show();
				}
			});
			return load;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Displaying the inscription window
	 */
	public static void displayInscription() {
		customer = new CustomerApplication(discount, rooms);
		CustomerApplication.displayInscription();
	}

	/**
	 * Displaying the home window
	 * 
	 * @param load FXMLLoader of the home
	 */
	public static void displayHome(FXMLLoader load) {
		HomeController hc = load.getController();
		displayMovie(rooms, hc);
		if (signOut) {
			CustomerApplication.displayHomeMember();
		}
	}

	/**
	 * Closing the stage
	 */
	public static void closeStage() {
		primaryStage.close();
	}

	/**
	 * 
	 * @return the boolean to know if it is a member or not
	 */
	public static boolean getSignOut() {
		return signOut;
	}

	/*
	 * Modify the state of the session
	 */
	public static void setSignOut(boolean sO) {
		signOut = sO;
	}

}
