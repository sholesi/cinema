package controller;

import java.sql.SQLException;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import model.ICustomer;
import model.IEmployee;
import model.BD.DAO.CalendarDAO;
import model.BD.DAO.CustomerDAO;
import model.BD.DAO.DiscountDAO;
import model.BD.DAO.MovieDAO;
import model.BD.DAO.RoomDAO;
import model.BD.ImpDAO.CalendarImpDAO;
import model.BD.ImpDAO.CustomerImpDAO;
import model.BD.ImpDAO.DiscountImpDAO;
import model.BD.ImpDAO.MovieImplDAO;
import model.BD.ImpDAO.RoomImpDAO;
import model.attribut.Discount;
import model.attribut.Movie;

public class EmployeeApplication {
	private static ArrayList<Movie> movies;
	private static String movieImages;
	private static ArrayList<Discount> discount;
	private static IEmployee employee;
	private static Movie lastMovieClicked;
	private static CustomerDAO<ICustomer> cust;
	private static MovieDAO movie;
	private static DiscountDAO disc;
	private static CalendarDAO calendar;
	private static RoomDAO roomD;


	public EmployeeApplication(ArrayList<Discount> discount_, IEmployee emp) {
		employee = emp;
		discount = discount_;
		cust = new CustomerImpDAO();
		movie = new MovieImplDAO();
		disc = new DiscountImpDAO();
		movies = movie.getListMovies();
		calendar = new CalendarImpDAO();
		roomD = new RoomImpDAO();
	}

        /**
         * put the last movie selected by the user
         * 
         * @param name of the movie
         */
	public static void setLastMovieClicked(String name) {
		lastMovieClicked = getLastMovieClicked(name);
	}

        /**
         * recovers the last movie selected by the user
         * 
         * @param name of the movie
         * @return res, the associated object Movie
         */
	public static Movie getLastMovieClicked(String name) {
		Movie res = null;
		for (int i = 0; i < movies.size(); i++) {
			if (movies.get(i).getName().equals(name)) {
				res = movies.get(i);
			}
		}

		return res;
	}

        /**
         * getting the list of all the movies
         * 
         * @return movies, the ArrayList containing of the movies
         */
	public static ArrayList<Movie> getListMovies() {
		return movies;
	}

        /**
         * getting the last movie clicked
         * @return 
         */
	public static Movie getLastMovieClicked() {
		return lastMovieClicked;
	}

        /**
         * Add the movie with all their associated information
         * 
         * @param name of the movie
         * @param price of the movie
         * @param desc of the movie
         * @param imageByte corresponding at the movie
         * @param dateB the date of the first appearance of the film in our cinema
         * @param dateF the date of the last appearance of the film in our cinema
         * @param hour the hours of projection of the movie
         * @param duration the length of the movie (in minutes)
         * @param room the room where will be displayed the movie
         */
	public static void addMovie(String name, double price, String desc, String imageByte, String dateB, String dateF, String hour, int duration, int room) {
		movies.add(movie.addMovie(name, price, desc, imageByte, dateB, dateF, hour, duration, room));
		movieImages = imageByte;

	}
        
        /**
         * Update the movie with all their associated information
         * 
         * @param firstName the name of the movie clicked on the list of all the movies
         * @param name of the movie
         * @param price of the movie
         * @param desc of the movie
         * @param imageByte corresponding at the movie
         * @param dateB the date of the first appearance of the film in our cinema
         * @param dateF the date of the last appearance of the film in our cinema
         * @param hour the hours of projection of the movie
         * @param duration the length of the movie (in minutes)
         */
	public static void updateMovie(String firstName, String name, double price, String desc, String imageByte,
			String dateB, String dateF, String hour, int duration) {
		movie.updateMovie(firstName, name, price, desc, imageByte, dateB, dateF, hour, duration);
	}

        /**
         * Add a new offer for the customers
         * 
         * @param name, the name of the offer
         * @param type, for whom is available the discount
         * @param red, the amount of the reduction
         */
	public static void addDiscount(String name, String type, double red) {
		disc.addDiscount(name, type, red);
	}

        /**
         * Getting the list of the three most popular movies, depending on the number of tickets purchased
         * 
         * @return a table of three movies
         * @throws SQLException 
         */
	public static Movie[] getPopularMovies() throws SQLException {
		Movie[] tabPopMovies = new Movie[3];

		tabPopMovies = movie.getPopularMovies();

		return tabPopMovies;
	}

        /**
         * changes the status of the movie
         * 
         * @param movieName, the name of the movie selected
         * @param status, the current status of the movie selected 
         */
	public static void changeStatus(String movieName, String status) {
		UPcheckMovie(movieName, status);
		movie.changeStatus(movieName, status);
	}

        /**
         * 
         * @param movieName
         * @param stat 
         */
	private static void UPcheckMovie(String movieName, String stat) {
		for (Movie mov : movies) {
			if (mov.getName().equals(movieName)) {
				if (stat.equals("disable")) {
					mov.setStatus(true);
				} else {
					mov.setStatus(false);
				}
			}
		}

	}

        /**
         * Getting the data related to the customers types
         * 
         * @return a list of data for a PieChart
         * @throws SQLException 
         */
	public static ObservableList<PieChart.Data> getDataCustomersType() throws SQLException {
		return cust.getDataCustomersType();
	}

        /**
         * display the window of the employee home
         */
	public static void displayHomeEmployee() {
		Cinema.changeScene("/view/employee/FXMLEmployee.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

        /**
         * Getting the list of all the members
         * 
         * @return the list of members
         */
	public static ArrayList<ICustomer> getListMembers() {
		return cust.getListMembers();
	}

        /**
         * Getting the hours of projection of a movie
         * 
         * @param idMovie, the id of the movie selected
         * @return a table of string of the the hours of the movie
         */
	public static String[] getHours(int idMovie) {
		return calendar.getHours(idMovie);
	}

        /**
         * Getting the id of the movie depending of its name*
         * 
         * @param name, the name of the movie chosen
         * @return the id of the movie
         */
	public static int getIdMovie(String name) {
		return movie.getIdMovie(name);
	}

        /**
         * Getting the id of the rooms of the cinema
         * 
         * @return an ArrayList containing all the id of the rooms
         */
	public static ArrayList<Integer> getIdRooms(){
          return roomD.getIdRooms();
        }


}
