package controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

import javafx.fxml.FXMLLoader;
import javafx.scene.image.ImageView;
import model.ICustomer;
import model.BD.DAO.CalendarDAO;
import model.BD.DAO.CustomerDAO;
import model.BD.DAO.TicketDAO;
import model.BD.ImpDAO.CalendarImpDAO;
import model.BD.ImpDAO.CustomerImpDAO;
import model.BD.ImpDAO.TicketImpDAO;
import model.attribut.Calendar;
import model.attribut.Discount;
import model.attribut.Room;
import model.attribut.Ticket;
import model.attribut.TypeC.typeC;
import model.user.Guest;
import model.user.Member;
import view.HomeController;
import view.customer.BuyMoviesController;
import view.customer.DisplayTicketsController;
import view.customer.InscriptionController;
import view.customer.PaymentController;
import view.customer.SuccessController;

public class CustomerApplication {
	private static ArrayList<Calendar> time;
	private static ArrayList<Discount> discount;
	private static ArrayList<Room> rooms;
	private static ICustomer customer;
	private static ArrayList<Ticket> tickets;
	private static CustomerDAO<ICustomer> custDAO;
	private static TicketDAO tickDAO;
	private static CalendarDAO calDAO;

	public CustomerApplication(ArrayList<Discount> disc, ArrayList<Room> room, ArrayList<Calendar> times,
			ICustomer cust) {
		calDAO = new CalendarImpDAO();
		tickDAO = new TicketImpDAO();
		custDAO = new CustomerImpDAO();
		tickets = new ArrayList<Ticket>();
		discount = disc;
		rooms = room;
		customer = cust;
		time = times;

	}

	public CustomerApplication(ArrayList<Discount> disc, ArrayList<Room> room) {
		calDAO = new CalendarImpDAO();
		tickDAO = new TicketImpDAO();
		custDAO = new CustomerImpDAO();
		tickets = new ArrayList<Ticket>();
		discount = disc;
		rooms = room;
		time = calDAO.getAllCalendar(rooms);
	}

	/**
	 * Displaying the window inscription
	 */
	public static void displayInscription() {
		FXMLLoader load = Cinema.changeScene("/view/customer/inscription.fxml", Cinema.getPrimaryStage(), false,
				"customer.css", true);
		InscriptionController ic = load.getController();

		for (int i = 0; i < typeC.values().length; i++) {
			if (typeC.values()[i] != typeC.All) {
				ic.initBoxDiscount(typeC.values()[i]);
			}
		}
	}

	/**
	 * Displaying the home of the member
	 */
	public static void displayHomeMember() {
		FXMLLoader load = Cinema.changeScene("/view/home.fxml", Cinema.getPrimaryStage(), false, "customer.css", true);
		HomeController hc = load.getController();
		hc.initM(((Member) customer).getLastName());
		Cinema.displayMovie(rooms, hc);

	}

	/**
	 * Creating a member
	 * 
	 * @param firstN first name of the member
	 * @param lastN  last name of the member
	 * @param login  login of the member
	 * @param pass   password of the member
	 * @param type   type of the member
	 */
	public static void createMember(String firstN, String lastN, String login, String pass, String type) {
		customer = new Member(firstN, lastN, login, pass, typeC.valueOf(type.toString()));
		custDAO.createCustomer(customer);
		displayHomeMember();
	}

	/**
	 * Displaying the window buyMovie
	 * 
	 * @param imageView image of the movie selected
	 */
	public void displayBuyMovie(ImageView imageView) {

		FXMLLoader load = Cinema.changeScene("/view/customer/buyMovies.fxml", Cinema.getPrimaryStage(), false,
				"customer.css", true);
		BuyMoviesController bv = load.getController();
		ArrayList<Calendar> cal = Cinema.getDateCalendar(Integer.parseInt(imageView.getId()));
		ArrayList<String> allHours = conversionStrigDate(cal);
		ArrayList<Integer> allIdDate = getIdDate(cal);

		
		bv.init(imageView, allIdDate);
		bv.initDate(cal.get(0).getDateFrom(), cal.get(0).getDateTo());
		String hourFromTab[];
		for (int j = 0; j < allHours.size(); j++) {
			hourFromTab = allHours.get(j).split(" ");
			bv.displayDate(hourFromTab);
		}

		if (!Cinema.getSignOut()) {
			bv.hideDiscount();

		} else {
			if (Cinema.getSignOut()) {
				for (int i = 0; i < discount.size(); i++) {
					bv.displayDiscount(discount.get(i).toString());
				}
			} else {
				bv.hideDiscount();
			}
		}
		for (int i = 1; i <= this.getRoomSelect(imageView.getId()).getSeat(); i++) {
			bv.displayPlace(i);
		}

		bv.setIdMovie(this.getRoomSelect(imageView.getId()).getMovie().getId());
		bv.setName(this.getRoomSelect(imageView.getId()).getMovie().getName());
	}

	/**
	 * Getting all the id of the dates of movies
	 * 
	 * @param cal dates of movies
	 * @return the list of id
	 */
	private ArrayList<Integer> getIdDate(ArrayList<Calendar> cal) {
		ArrayList<Integer> id = new ArrayList<Integer>();
		String hourFromTab[];
		for (int i = 0; i < cal.size(); i++) {
			hourFromTab = cal.get(i).getHour().split(" ");
			for (int j = 0; j < hourFromTab.length; j++) {
				id.add(cal.get(i).getId());
			}
		}
		return id;
	}

	/**
	 * Converting the dates of movies to string
	 * 
	 * @param cal dates of movies
	 * @return dates converted
	 */
	private ArrayList<String> conversionStrigDate(ArrayList<Calendar> cal) {
		ArrayList<String> date = new ArrayList<String>();

		for (int i = 0; i < cal.size(); i++) {
			date.add(cal.get(i).getHour());
		}

		return date;
	}

	/**
	 * Creating a guest
	 */
	public static void createGuest() {
		customer = new Guest();
		custDAO.createCustomer(customer);
	}

	/**
	 * Getting the rooms selected
	 * 
	 * @param idRoom id of the room
	 * @return the rooms selected
	 */
	public Room getRoomSelect(String idRoom) {
		for (Room room : rooms) {
			if (Integer.parseInt(idRoom) == room.getID()) {
				return room;
			}
		}
		return null;
	}

	/**
	 * Buying movie
	 * 
	 * @param idRoom   id of the rooms
	 * @param cal      id of the calendar(obtain the date range) selected
	 * @param nbPlace  number of seat
	 * @param discount discount selected
	 * @param object
	 */
	public static void buyMovie(String idRoom, int cal, String date, String nbPlace, String discount) {

		if (discount != null) {
			String disc = discount.split(":")[0];
			tickets.add(customer.purchaseMovie(getCalendarSelect(cal), date, Integer.parseInt(nbPlace),
					getDiscCustomer(discount), getDiscCustomerWant(disc)));
		} else {
			tickets.add(customer.purchaseMovie(getCalendarSelect(cal), date, Integer.parseInt(nbPlace), null, null));
		}
		FXMLLoader load = Cinema.changeScene("/view/customer/payment.fxml", Cinema.getPrimaryStage(), false,
				"customer.css", false);
		PaymentController pc = load.getController();
		pc.init();
	}

	/**
	 * Getting the data range of the movie
	 * 
	 * @param cal id of the calendar (data range)
	 * @return the data range selected
	 */
	private static Calendar getCalendarSelect(int cal) {
		for (Calendar timeD : time) {
			if (cal == timeD.getId()) {
				System.out.println(timeD.getRoom().getMovie().getPrice());
				return timeD;
			}
		}
		return null;
	}

	/**
	 * Getting the room selected
	 * 
	 * @param idRoom id of the room
	 * @return the room
	 */
	public static Room getMovieSelect(String idRoom) {
		for (Room room : rooms) {
			if (Integer.parseInt(idRoom) == room.getID()) {
				return room;
			}
		}
		return null;
	}

	/**
	 * Getting the list of the discount linked to the customers
	 * 
	 * @param disco type discount
	 * @return the list of the discount
	 */
	public static ArrayList<Discount> getDiscCustomer(String disco) {
		ArrayList<Discount> dis = new ArrayList<Discount>();
		for (Discount disc : discount) {
			if (disc.getType() == customer.getType()) {
				dis.add(disc);
			}
		}
		return dis;
	}

	/**
	 * Getting the discount that the customer want
	 * 
	 * @param disco type discount
	 * @return disount selected
	 */
	public static Discount getDiscCustomerWant(String disco) {
		for (Discount disc : discount) {
			if (disc.getName().equals(disco)) {
				return disc;
			}
		}
		return null;
	}

	/**
	 * Displaying window success
	 */
	public static void displaySuccess() {
		Ticket ticket = tickets.get(tickets.size() - 1);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d h:mma", new Locale("en"));
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd MMMM YYYY h:mma", new Locale("en"));
		LocalDateTime dateT = LocalDateTime.parse(ticket.getDate().toString(), formatter);
		String date = dateT.format(formatter2);
		FXMLLoader load = Cinema.changeScene("/view/customer/success.fxml", Cinema.getPrimaryStage(), false,
				"customer.css", false);
		SuccessController sc = load.getController();
		if (Cinema.getSignOut()) {
			sc.init(date, ticket.getPrice(), capitalString(ticket.getMovie().getName()),
					capitalString(ticket.getRoom().getName()), Integer.toString(ticket.getNbPlace()),
					capitalString(((Member) ticket.getCustumer()).getFirstName() + " "
							+ ((Member) ticket.getCustumer()).getLastName()),
					Integer.toString(ticket.getId()), true);
		} else {
			sc.init(date, ticket.getPrice(), capitalString(ticket.getMovie().getName()),
					capitalString(ticket.getRoom().getName()), Integer.toString(ticket.getNbPlace()),
					Integer.toString(ticket.getCustumer().getId()), Integer.toString(ticket.getId()), false);
		}

	}

	/**
	 * Putting the first letter to the upperCase
	 * 
	 * @param world world concerned
	 * @return the word modified
	 */
	public static String capitalString(String world) {
		char[] arr = world.toCharArray();
		arr[0] = Character.toUpperCase(arr[0]);
		return new String(arr);

	}

	/**
	 * Displaying tickets
	 */
	public static void displayAllTickets() {
		FXMLLoader load = Cinema.changeScene("/view/customer/displayTickets.fxml", Cinema.getPrimaryStage(), false,
				"customer.css", true);
		DisplayTicketsController dc = load.getController();
		dc.addImage(tickDAO.getImages(customer.getId()));

	}

	/**
	 * Adding a url of the ticket's image
	 * 
	 * @param url url of the image
	 */
	public static void addURLImage(String url) {
		tickets.get(tickets.size() - 1).setUrl(url);
		tickDAO.updateImage(tickets.get(tickets.size() - 1));
	}

	/**
	 * Disconnecting a member
	 */
	public static void disconnect() {
		customer = null;
		Cinema.setSignOut(false);
		FXMLLoader load = Cinema.changeScene("/view/home.fxml", Cinema.getPrimaryStage(), false, "customer.css", true);
		Cinema.displayHome(load);

	}

	/**
	 * Modify the customer
	 * 
	 * @param disc discount of the customer
	 * @param cust customer
	 */
	public void setCustomer(ArrayList<Discount> disc, ICustomer cust) {
		customer = cust;
		discount = disc;
		tickets = null;
	}

}
