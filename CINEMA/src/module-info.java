module cinema {
	requires java.sql;
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires javafx.base;
	requires java.desktop;
	requires javafx.swing;
	requires java.base;

	
	opens controller to javafx.graphics, javafx.fxml,java.sql,javafx.controls;
	opens model to javafx.graphics, javafx.fxml,java.sql,javafx.controls;
	opens view to javafx.graphics, javafx.fxml,java.sql,javafx.controls;
	opens view.customer to javafx.graphics, javafx.fxml,java.sql,javafx.controls;
	opens view.employee to javafx.graphics, javafx.fxml,java.sql,javafx.controls;
}