package view.customer;

import java.io.IOException;

import java.util.List;

import controller.Cinema;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class DisplayTicketsController implements EventHandler {
	@FXML
	private ImageView idHome;
	@FXML
	private ImageView idClose;
	@FXML
	private ScrollPane idScroll;

	/**
	 * Going to window home
	 * 
	 * @param event click on the image home
	 */
	@FXML
	void backHome(MouseEvent event) {
		FXMLLoader load = Cinema.changeScene("/view/home.fxml", Cinema.getPrimaryStage(), false,"customer.css",true);
		Cinema.displayHome(load);
	}

	/**
	 * Going to window home
	 * 
	 * @param event click on the image home
	 */
	@FXML
	void close(MouseEvent event) {
		Cinema.closeStage();
	}

	/**
	 * Adding the images of tickets
	 * @param images list images
	 */
	public void addImage(List<Image> images) {
		GridPane gp = new GridPane();
		this.idScroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
		this.idScroll.setVbarPolicy(ScrollBarPolicy.NEVER);


		int size = (int) Math.sqrt(images.size()) + 1;
		ImageView im;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				im = new ImageView();
				im.setFitHeight(100);
				im.setFitWidth(200);
				im.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
				gp.add(im, i, j);
			}
		}
		for (int u = 0; u < images.size(); u++) {
			((ImageView) gp.getChildren().get(u)).setImage(images.get(u));
		}
		this.idScroll.setContent(gp);
	}

	@Override
	public void handle(Event e) {

		try {
			ImageView im=new ImageView();
			im.setImage(((ImageView) e.getSource()).getImage());
			FXMLLoader load = new FXMLLoader(getClass().getResource("/view/customer/image.fxml"));
			BorderPane root = (BorderPane) load.load();
			ImageController ic = load.getController();
			ic.init(im);
			((ImageView) e.getSource()).getFitHeight();
			((ImageView) e.getSource()).getFitWidth();
			Stage stage = new Stage();
			Scene scene=new Scene(root);
			scene.setFill(Color.TRANSPARENT);
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

}
