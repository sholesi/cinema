package view.customer;

import java.io.File;

import java.io.IOException;
import javax.imageio.ImageIO;
import controller.Cinema;
import controller.CustomerApplication;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

public class SuccessController {
	@FXML
	private BorderPane idBorderPrinc;

	@FXML
	private ImageView idHome;

	@FXML
	private BorderPane idBord2;

	@FXML
	private Text idCode2;

	@FXML
	private Text nameFilm;

	@FXML
	private Text idDate;

	@FXML
	private Text idRoom;

	@FXML
	private Text idNameCust;

	@FXML
	private Text idSeat;

	@FXML
	private Text idPrice;

	@FXML
	private Text idCode;

	@FXML
	private ImageView idClose;


	/**
	 * Going to window home
	 * 
	 * @param event click on the image home
	 */
	@FXML
	void backHome(MouseEvent event) {
		FXMLLoader load = Cinema.changeScene("/view/home.fxml", Cinema.getPrimaryStage(), false,"customer.css",true);
		Cinema.displayHome(load);
	}

	/**
	 * Initialize the ticket
	 * @param date date of the purchase
	 * @param price proce of the purchase
	 * @param nameMovie name of the movie chosen
	 * @param nameRoom name of the room chosen
	 * @param seat number of seat
	 * @param name name of the customer
	 * @param idTicket id of the ticket 
	 * @param takeP knowing if we have to take a picture of the scene for the member
	 */
	public void init(String date, double price, String nameMovie, String nameRoom, String seat,
			String name, String idTicket, boolean takeP) {
		this.idDate.setText(date);
		this.nameFilm.setText(nameMovie);
		this.idRoom.setText(nameRoom);
		this.idSeat.setText("seat: " + seat);
		this.idNameCust.setText(name);
		this.idPrice.setText(Double.toString(price) + " �");

		int range = (10000 - 10) + 1;
		int code = (int) (Math.random() * range) + 10;
		this.idCode.setText(Integer.toString(code));
		this.idCode2.setText(Integer.toString(code));
		this.idBorderPrinc.setId("removeBack");
		if(takeP) {
			this.takePicture(idTicket);
		}

	}

	/**
	 * Taking picture of the ticket for member
	 * @param idTicket id of the ticket
	 */
	private void takePicture(String idTicket) {

		Platform.runLater(new Runnable() {
			@Override
			public void run() {

				idHome.setOpacity(0);
				idClose.setOpacity(0);
				WritableImage image = idBord2.getScene().snapshot(null);
				File file = new File("tickets/" + idTicket + ".png");
				idHome.setOpacity(1);
				idClose.setOpacity(1);
				try {
					ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
					CustomerApplication.addURLImage("tickets/" + idTicket + ".png");
				} catch (IOException e) {

				}
			}
		});

	}

	/**
	 * Closing stage
	 * 
	 * @param event click on the cross
	 */
	@FXML
	void close(MouseEvent event) {
		Cinema.closeStage();
	}

}
