package view.customer;

import javafx.fxml.FXML;


import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import model.attribut.TypeC.typeC;
import controller.Cinema;
import controller.CustomerApplication;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;

public class InscriptionController {
	@FXML
	private ImageView idHome;
	@FXML
	private TextField idFirstN;
	@FXML
	private TextField idLastN;
	@FXML
	private TextField idLogin;
	@FXML
	private TextField idPass;
	@FXML
	private ComboBox<typeC> idType;

	/**
	 * Creating a member
	 * 
	 * @param event click on a the sign up
	 */
	@FXML
	public void createCustumer(ActionEvent event) {
		if (!this.idLastN.getText().isEmpty() && !this.idFirstN.getText().isEmpty() && !this.idLogin.getText().isEmpty()
				&& !this.idPass.getText().isEmpty()) {
			CustomerApplication.createMember(idFirstN.getText(), idLastN.getText(), idLogin.getText(), idPass.getText(),
					idType.getValue().toString());
		}
	}

	/**
	 * Initalize the type of customer possible
	 * 
	 * @param typeC type of customer
	 */
	public void initBoxDiscount(typeC typeC) {
		this.idType.getItems().add(typeC);
		this.idType.setValue(typeC);

	}

	/**
	 * Going to window home
	 * 
	 * @param event click on the image home
	 */
	@FXML
	void backHome(MouseEvent event) {
		FXMLLoader load = Cinema.changeScene("/view/home.fxml", Cinema.getPrimaryStage(), false, "customer.css",true);
		Cinema.displayHome(load);
	}

	/**
	 * Closing stage
	 * 
	 * @param event click on the cross
	 */
	@FXML
	void close(MouseEvent event) {
		Cinema.closeStage();
	}
}
