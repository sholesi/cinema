package view.customer;

import java.time.LocalDate;
import java.util.ArrayList;

import controller.Cinema;

import controller.CustomerApplication;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

public class BuyMoviesController {
	@FXML
	private BorderPane idBoxPrinc;
	@FXML
	private HBox idBar;
	@FXML
	private ImageView idHome;

	@FXML
	private ComboBox<Integer> idPlace;
	@FXML
	private ComboBox<String> idDiscount;
	@FXML
	private Button idChoose;
	@FXML
	private Label idLDiscount;
	@FXML
	private ImageView idImage;
	@FXML
	private Label idLMovie;
	@FXML
	private ImageView idClose;
	@FXML
	private ComboBox<String> idDate_;
	@FXML
	private DatePicker idDate;
	private ArrayList<Integer> id = new ArrayList<Integer>();

	/**
	 * Displaying the window loading
	 * 
	 * @param event clik on the button
	 */
	@FXML
	void displayPayment(ActionEvent event) {
		if (!Cinema.getSignOut()) {
			CustomerApplication.buyMovie(this.idImage.getId(), id.get(idDate_.getSelectionModel().getSelectedIndex()),
					this.idDate.getValue()+" "+this.idDate_.getValue(), this.idPlace.getValue().toString(), null);
		} else {

			CustomerApplication.buyMovie(this.idImage.getId(), id.get(idDate_.getSelectionModel().getSelectedIndex()),
					this.idDate.getValue()+" "+this.idDate_.getValue(), this.idPlace.getValue().toString(), this.idDiscount.getValue().toString());
		}
	}

	/**
	 * Displaying the discounts
	 * 
	 * @param discount name of the discount
	 */
	public void displayDiscount(String discount) {

		idDiscount.getItems().add(discount);
		idDiscount.setValue(discount);
	}

	/**
	 * Display number of seat
	 * 
	 * @param indice number seat
	 */
	public void displayPlace(int indice) {
		idPlace.getItems().add(indice);
		idPlace.setValue(1);
	}

	/**
	 * Hiding discount for the guest
	 */
	public void hideDiscount() {
		idDiscount.setVisible(false);
		this.idLDiscount.setVisible(false);
	}

	/**
	 * Setting the movie
	 * 
	 * @param id id of the movie
	 */
	public void setIdMovie(int id) {
		this.idImage.setId(Integer.toString(id));
	}

	/**
	 * Setting name
	 * 
	 * @param name name of the movie
	 */
	public void setName(String name) {
		this.idLMovie.setText(name);

	}

	/**
	 * Going to window home
	 * 
	 * @param event click on the image home
	 */
	@FXML
	void backHome(MouseEvent event) {
		FXMLLoader load = Cinema.changeScene("/view/home.fxml", Cinema.getPrimaryStage(), false, "customer.css",true);
		Cinema.displayHome(load);
	}

	/**
	 * Closing stage
	 * 
	 * @param event click on the cross
	 */
	@FXML
	void close(MouseEvent event) {
		Cinema.closeStage();
	}

	/**
	 * Initialize the window
	 * 
	 * @param image     image
	 * @param allIdDate dates related to the rooms
	 */
	public void init(ImageView image, ArrayList<Integer> allIdDate) {
		this.idImage.setImage(image.getImage());
		this.id.addAll(allIdDate);
	}

	/**
	 * Displaying date
	 * 
	 * @param hourFromTab date range
	 */
	public void displayDate(String[] hourFromTab) {
		idDate_.getItems().addAll(hourFromTab);
		idDate_.setValue(hourFromTab[0]);
	

	}

	/**
	 * Initialize date picker 
	 * @param dateFrom beginning date allowed
	 * @param dateTo ending date allowed
	 */
	public void initDate(LocalDate dateFrom, LocalDate dateTo) {
		DatePicker maxDate = new DatePicker();
		maxDate.setValue(dateTo);
		DatePicker minDate = new DatePicker();
		minDate.setValue(dateFrom);
		final Callback<DatePicker, DateCell> dayCellFactory;

		dayCellFactory = (final DatePicker datePicker) -> new DateCell() {
			@Override
			public void updateItem(LocalDate item, boolean empty) {
				super.updateItem(item, empty);
				if (item.isAfter(maxDate.getValue())) {
					setDisable(true);
					setStyle("-fx-background-color: #ffc0cb;");
				}

				if (item.isBefore(minDate.getValue())) {
					setDisable(true);
					setStyle("-fx-background-color: #ffc0cb;");
				}
			}
		};

		this.idDate.setDayCellFactory(dayCellFactory);

	}

}
