package view.customer;



import controller.Cinema;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;


public class IdentifyController {
    @FXML
    private ImageView idHome;
	@FXML
	private TextField idLogin;
	@FXML
	private TextField idPass;
    @FXML
    private Label idError;
	@FXML
	private Button idLogIn;
	
    @FXML
    private Button signUp;
    @FXML
    private ImageView idClose;

    /**
     * Displaying window inscription
     * @param event click on the button
     */
    @FXML
    void displayInscription(ActionEvent event) {
    	Cinema.displayInscription();
    }

    /**
     * Authentify the person
     * @param event click on a button
     */
    @FXML
    void autentification(ActionEvent event) {
    	Cinema.autentification(idLogin.getText(), idPass.getText(),this);
    }
    
    /**
     * Displaying error if the login or password is not good
     */
	public void displayError() {
		this.idLogin.setStyle("-fx-border-color:#e8c103;-fx-background-color:#801d16");
		this.idPass.setStyle("-fx-border-color:#e8c103;-fx-background-color:#801d16");
		this.idError.setText("It's not the good login or password");
	}
	
	/**
	 * Going to window home
	 * 
	 * @param event click on the image home
	 */
    @FXML
    void backHome(MouseEvent event) {
    	FXMLLoader load=Cinema.changeScene("/view/home.fxml", Cinema.getPrimaryStage(),false,"customer.css",true);
    	Cinema.displayHome(load);
    }
    

	/**
	 * Closing stage
	 * 
	 * @param event click on the cross
	 */
    @FXML
    void close(MouseEvent event) {
    	Cinema.closeStage();
    }
}
