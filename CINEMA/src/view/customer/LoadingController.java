package view.customer;

import java.util.Timer;
import java.util.TimerTask;

import controller.CustomerApplication;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

public class LoadingController extends TimerTask {

	@FXML
	private ProgressIndicator idProgress;
	private Timer time;
    @FXML
    private Text idText;

    @FXML
    private BorderPane idBox;


	@Override
	public void run() {
		time.cancel();
		CustomerApplication.displaySuccess();

	}

	/**
	 * Initialize the progress bar and timer 
	 */
	public void initialize() {
		this.idProgress.setOpacity(0);;
		this.idProgress.setStyle("-fx-progress-color: #801d16");
		this.idProgress.setMinWidth(500);
		this.idProgress.setMinHeight(500);
		this.idText.setId("textLoad");
		time = new Timer();
		time.schedule(new TimerTask() {

			@Override
			public void run() {
				idProgress.setOpacity(idProgress.getOpacity()+0.1);
				
			}},0, 1000);
		
		time.schedule(this, 10000);

	}

}
