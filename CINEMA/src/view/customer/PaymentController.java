package view.customer;

import javafx.scene.control.TextField;

import javafx.scene.input.KeyEvent;

import javafx.scene.input.MouseEvent;
import controller.Cinema;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class PaymentController implements EventHandler {

	@FXML
	private Text idValid;
	@FXML
	private TextField idnum;

	@FXML
	private TextField idExpire;

	@FXML
	private TextField idName;

	@FXML
	void validationPay(MouseEvent event) {
	
		if (this.checkValue()) {
			Cinema.changeScene("/view/customer/loading.fxml", Cinema.getPrimaryStage(), false, "customer.css",false);
		}

	}
	
	@FXML
	void checkNumber(ActionEvent event) {
		
	}


	@FXML
	void changeColor(MouseEvent event) {
		if (this.checkValue()) {
			this.idValid.setFill(Color.GREEN);
		}
	}

	private boolean checkValue() {
		if (this.idnum.getText().length() == 16 && this.idExpire.getText().length() == 5 && this.idName != null) {
			return true;
		}
		return false;
	}

	@FXML
	void changeColorS(MouseEvent event) {
		this.idValid.setFill(Color.WHITE);
	}


	public void init() {
		this.idnum.addEventHandler(KeyEvent.KEY_RELEASED, this);
		this.idName.setId("gold");
		this.idExpire.setId("gold");
		this.idnum.setId("gold");
		this.idExpire.addEventHandler(KeyEvent.KEY_RELEASED, this);
	}

	@Override
	public void handle(Event e) {
		KeyEvent ke = (KeyEvent) e;
		try {
			int c = (int) ke.getText().charAt(0);
			if (e.getSource() == this.idnum) {

				if (((c < 48) || (c > 57))) {
					this.idnum.setText(((KeyEvent) e).getText().replaceAll("[^\\d-]", " "));
				}
			}

			if (e.getSource() == this.idExpire) {

				if (((c < 48) || (c > 59))) {
					this.idExpire.setText(((KeyEvent) e).getText().replaceAll("[^\\d-]", " "));
				}
			}
		} catch (StringIndexOutOfBoundsException ex) {

		}
	}

}
