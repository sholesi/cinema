package view.customer;

import javafx.stage.Stage;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

public class ImageController {
	@FXML
	private VBox idBox_;


	/**
	 * Initialize the image ticket selected 
	 * @param im image 
	 */
	public void init(ImageView im) {
		idBox_.getChildren().add(im);
	}


	/**
	 * Closing stage
	 * 
	 * @param event click on the cross
	 */
	@FXML
	void close(MouseEvent event) {
		Stage s=(Stage)this.idBox_.getScene().getWindow();
		s.close();
	}
}
