package view;

import java.net.URL;
import java.util.ResourceBundle;

import controller.Cinema;

import controller.CustomerApplication;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class HomeController implements EventHandler, Initializable {

	@FXML
	private BorderPane boxPric;
	@FXML
	private Button idLogIn;
	@FXML
	private Button idChoose;
	@FXML
	private VBox idBoxC;
	@FXML
	private Label idName;
	@FXML
	private HBox idBox3;
	@FXML
	private HBox idBox4;
	@FXML
	private ImageView idClose;
	private Button idTickets;
	private Button idDisc;

	@FXML
	private ScrollPane idScroll;

	private HBox idBox2=new HBox();

	/**
	 * Displaying the window identify
	 * 
	 * @param event a click on log in
	 */
	@FXML
	void displayIdentify(ActionEvent event) {
		Cinema.changeScene("/view/customer/identify.fxml", Cinema.getPrimaryStage(), false, "customer.css", true);
	}

	/**
	 * Displaying the buttons related to the member
	 * 
	 * @param lastName the lastName of the member
	 */
	public void initM(String lastName) {
		VBox box = new VBox();
		this.idTickets = new Button();
		idTickets.setMinWidth(152);
		idTickets.setMinHeight(62);
		idTickets.setText("See your tickets");
		idTickets.setId("ticket");
		idTickets.addEventHandler(MouseEvent.MOUSE_CLICKED, this);

		this.idDisc = new Button();
		idDisc.setText("Sign out");
		idDisc.setMinWidth(82);
		idDisc.setMinHeight(62);
		idDisc.setId("disc");
		idDisc.addEventHandler(MouseEvent.MOUSE_CLICKED, this);

		box.setMargin(idTickets, new Insets(20, 450, 0, 40));
		box.getChildren().add(idTickets);

		this.idBox3.setAlignment(Pos.TOP_LEFT);
		this.idBox3.getChildren().remove(idLogIn);
		this.idBox3.getChildren().add(box);
		this.idBox3.getChildren().add(idDisc);
		this.idBox3.setMargin(idDisc, new Insets(20, 0, 0, 0));

		this.idName.setText("Welcome Mr./Ms. " + lastName);
	}

	/**
	 * Adding image
	 * 
	 * @param name         name of the movie
	 * @param id           id of the room concerned
	 * @param visible      knowing if the ticket sale is complete or not
	 * @param url          url of the image related to the movie
	 * @param isInProgress knowing if the movie is in progress or not
	 */
	public void addImage(String name, int id, boolean visible, String url, boolean isInProgress) {
		StackPane stack = null;

		Label text = new Label(name);
		text.setId("textImages");

		VBox group = new VBox();
		group.setId("image");

		Image im = new Image("file:" + url);
		ImageView iv = new ImageView(im);

		iv.setId(Integer.toString(id));
		iv.setFitHeight(im.getHeight() / 9);
		iv.setFitWidth(im.getWidth() / 9);
		iv.addEventHandler(MouseEvent.MOUSE_CLICKED, this);

		HBox g = new HBox();
		g.setId("boxImage");
		g.getChildren().add(iv);
		HBox.setMargin(group, new Insets(5, 0, 5, 10));

		if (!visible) {
			iv.setDisable(true);
			iv.setOpacity(0.3);
			Label notAV = new Label("COMPLETE");
			notAV.setId("notAvailable");
			notAV.setRotate(-20);
			stack = new StackPane();
			stack.getChildren().add(g);
			stack.getChildren().add(notAV);
			group.getChildren().add(stack);
		} else {

			if (isInProgress) {
				iv.setOpacity(0.6);
				Label inPro = new Label("IN PROGRESS");
				inPro.setId("progress");
				inPro.setRotate(-20);
				stack = new StackPane();
				stack.getChildren().add(g);
				stack.getChildren().add(inPro);
				group.getChildren().add(stack);
			} else {
				group.getChildren().add(g);
			}
		}
		group.getChildren().add(text);

		this.boxPric.setId("boxImages");
		this.idBox2.getChildren().add(group);

		this.idScroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
		this.idScroll.setVbarPolicy(ScrollBarPolicy.NEVER);
		this.idScroll.setContent(idBox2);

	}

	@Override
	public void handle(Event e) {

		if (e.getSource().getClass() == ImageView.class) {
			Cinema.displayBuyMovie(((ImageView) e.getSource()));
		}
		if (e.getSource() == this.idDisc) {
			CustomerApplication.disconnect();
		}

		if (e.getSource() == this.idTickets) {
			CustomerApplication.displayAllTickets();
		}
	}

	/**
	 * Closing the stage
	 * 
	 * @param event click on the cross
	 */
	@FXML
	void close(MouseEvent event) {
		Cinema.closeStage();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.boxPric.setBackground(Background.EMPTY);
		this.idBoxC.setId("pane");
	}



}
