package view.employee;

import java.io.IOException;

import controller.Cinema;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import javafx.scene.layout.GridPane;

public class FXMLEmployeeController {

	@FXML
	private GridPane gridPane;

	@FXML
	private Button buttonNewMovie;

	@FXML
	private Button buttonDisplayPopMovie;

	@FXML
	private Button buttonCustRecords;

	@FXML
	private Button buttonUpdateMovie;

	@FXML
	private Button buttonNewOffer;

	@FXML
	private Button buttonNewMobuttonUpdateMovieStatevie1;
	@FXML
	private ImageView close;

        /**
         * move to the window for adding movies
         * 
         * @param event
         * @throws IOException 
         */
	@FXML
	void addNewMovie(ActionEvent event) throws IOException {

		Cinema.changeScene("/view/employee/FXMLAddMovie.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

        /**
         *  move to the window for adding offers
         * 
         * @param event
         * @throws IOException 
         */
	@FXML
	void addNewOffer(ActionEvent event) throws IOException {
		Cinema.changeScene("/view/employee/FXMLAddOffer.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

        /**
         * move to the window for displaying customers records
         * 
         * @param event
         * @throws IOException 
         */
	@FXML
	void displayCustomerRecords(ActionEvent event) throws IOException {

		Cinema.changeScene("/view/employee/FXMLCustomerRecords.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

        /**
         * move to the window for displaying popular movies
         * 
         * @param event
         * @throws IOException 
         */
	@FXML
	void displayPopularMovies(ActionEvent event) throws IOException {

		Cinema.changeScene("/view/employee/FXMLPopularMovies.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

	@FXML
	void updateMovie(ActionEvent event) throws IOException {

		Cinema.changeScene("/view/employee/FXMLListMovies.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

        /**
         * move to the window for updating movie state
         * 
         * @param event
         * @throws IOException 
         */
	@FXML
	void updateMovieState(ActionEvent event) throws IOException {

		Cinema.changeScene("/view/employee/FXMLUpdateState.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

        /**
        * Closing the stage 
        * @param event click on the cross
        */
	@FXML
	void close(MouseEvent event) {
		Cinema.closeStage();
	}

}
