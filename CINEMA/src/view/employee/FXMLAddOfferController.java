package view.employee;

import controller.Cinema;

import controller.EmployeeApplication;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.attribut.TypeC.typeC;

public class FXMLAddOfferController implements Initializable {

    @FXML
    private GridPane gridPane;

    @FXML
    private Label labelName;

    @FXML
    private Label labelDesc;

    @FXML
    private TextField textName;
    
    @FXML
    private ComboBox<typeC> textType;

    @FXML
    private Label labelRed;

    @FXML
    private TextField textRed;

    @FXML
    private Button buttonSubmit;

    @FXML
    private Label labelTitle;
    
    @FXML
    private Button buttonReturn;
    
    @FXML
    private ImageView buttonClose;
    
    
    /**
    * return to the previous window
    * 
    * @param event clik on the button
    */
    @FXML
    void actionReturn(ActionEvent event) throws IOException {
    	
    	Cinema.changeScene("/view/employee/FXMLEmployee.fxml", Cinema.getPrimaryStage(), true,"employee.css",false);
        
    }
    
    /**
    * add a new discount
    * 
    * @param event clik on the button
    */
    @FXML
    void addOffer(ActionEvent event) {
      
        String name = textName.getText();
        double reduction = Double.parseDouble(textRed.getText());
        String type = textType.getValue().toString();
        
        EmployeeApplication.addDiscount(name, type, reduction);
                


        Stage newStage = new Stage();
        BorderPane comp = new BorderPane();
        comp.setCenter(new Label("Discount added successfully."));

        Scene stageScene = new Scene(comp, 300, 100);
        newStage.setScene(stageScene);
        newStage.show();
    }

    /**
    * initialize the combobox of customers types
    */
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
    	this.textType.getItems().addAll(typeC.values());
    }
        
    /**
    * Closing the stage 
    * @param event click on the cross
    */
    @FXML
    void close(MouseEvent event) {
        Cinema.closeStage();
    }

}
