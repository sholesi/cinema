package view.employee;

import controller.Cinema;

import controller.EmployeeApplication;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import model.user.Member;

public class FXMLCustomerRecordsController implements Initializable {

	@FXML
	private ListView<String> listView;

	@FXML
	private Label labelTitle;

	@FXML
	private Button buttonReturn;

	@FXML
	private Button buttonCharts;
        
        @FXML
        private ImageView buttonClose;

        /**
        * initialize the data to display the list of customers
        * 
        * @param location 
        * @param resources
        */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loadData();
	}

        /**
        * load the data about the list of members
        * 
        */
	private void loadData() {

		ArrayList<String> members = new ArrayList<String>();
		for (int i = 0; i < EmployeeApplication.getListMembers().size(); i++) {
			String m = ((Member) EmployeeApplication.getListMembers().get(i)).getFirstName() + " "
					+ ((Member) EmployeeApplication.getListMembers().get(i)).getLastName() + ", "
					+ EmployeeApplication.getListMembers().get(i).getType().name();
			members.add(m);

		}

		listView.getItems().addAll(members);

	}

        /**
        * change window to display the chart about the repartition of tickets purchased by type of customer
        * 
        * @param event clik on the button
        */
	@FXML
	void displayCharts(ActionEvent event) throws IOException {
		
		Cinema.changeScene("/view/employee/FXMLCharts.fxml", Cinema.getPrimaryStage(), true,"employee.css",false);
		
	}

        /**
        * return to the previous window
        * 
        * @param event clik on the button
        */
	@FXML
	void actionReturn(ActionEvent event) throws IOException {
		Cinema.changeScene("/view/employee/FXMLEmployee.fxml", Cinema.getPrimaryStage(), true,"employee.css",false);
		
	}
        
        /**
        * Closing the stage 
        * @param event click on the cross
        */
        @FXML
        void close(MouseEvent event) {
            Cinema.closeStage();
        }

}
