/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.employee;

import controller.Cinema;

import controller.EmployeeApplication;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;

public class FXMLChartsController implements Initializable {

	@FXML
	private PieChart piechart;

	@FXML
	private Button buttonReturn;
        
        /**
        * return to the previous window
        * 
        * @param event clik on the button
        */
	@FXML
	void actionReturn(ActionEvent event) throws IOException {

		Cinema.changeScene("/view/employee/FXMLCustomerRecords.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

        /**
        * updload the data used for the piechart and display it
        * 
        * @param event clik on the button
        */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		try {
			ObservableList<PieChart.Data> data;

			data = EmployeeApplication.getDataCustomersType();

			piechart.setData(data);
		} catch (SQLException ex) {
			Logger.getLogger(FXMLChartsController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
