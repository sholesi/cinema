package view.employee;

import controller.Cinema;

import controller.EmployeeApplication;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import model.attribut.Movie;

public class FXMLPopularMoviesController implements Initializable{

    @FXML
    private Label labelTitle;

    @FXML
    private Button buttonReturn;

    @FXML
    private GridPane gridPane;

    @FXML
    private ImageView imageMovie1;

    @FXML
    private ImageView imageMovie2;

    @FXML
    private ImageView imageMovie3;

    @FXML
    private Label labelMovie1;

    @FXML
    private Label labelNum1;

    @FXML
    private Label labelNum2;

    @FXML
    private Label labelNum3;

    @FXML
    private Label labelMovie2;

    @FXML
    private Label labelMovie3;
    
    @FXML
    private ImageView buttonClose;

    /**
    * load the data at the loading of the window
    * @param location
    * @param resources 
    */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            setInformation();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLPopularMoviesController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FXMLPopularMoviesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * recovers the data related to the 3 most popular movies (image, name and tickets purchased)
     * and displays them
     * 
     * @throws SQLException
     * @throws IOException 
     */
    public void setInformation() throws SQLException, IOException {
        
       
        Movie[] tabPopMovies = new Movie[3];
        
        tabPopMovies = EmployeeApplication.getPopularMovies();
       
        //1st movie
        System.out.println(tabPopMovies[0].getUrl());
        Image img = new Image("file:"+tabPopMovies[0].getUrl());
        this.labelMovie1.setText(tabPopMovies[0].getName());
        this.labelNum1.setText(tabPopMovies[0].getTicketPurchase()+" tickets sold");               
        this.imageMovie1.setImage(img);
        
        //2nd movie
        Image img2 = new Image("file:"+tabPopMovies[1].getUrl());
        this.labelMovie2.setText(tabPopMovies[1].getName());
        this.labelNum2.setText(tabPopMovies[1].getTicketPurchase()+" tickets sold");
        this.imageMovie2.setImage(img2);
        
        //3rd movie
        Image img3 = new Image("file:"+tabPopMovies[2].getUrl());
        this.labelMovie3.setText(tabPopMovies[2].getName());
        this.labelNum3.setText(tabPopMovies[2].getTicketPurchase()+" tickets sold");
        this.imageMovie3.setImage(img3);
    }
    
    /**
    * come back to the previous window
    * 
    * @param event click on the button
    */
    @FXML
    void actionReturn(ActionEvent event) throws IOException {
   	 Cinema.changeScene("/view/employee/FXMLEmployee.fxml", Cinema.getPrimaryStage(), true,"employee.css",false);
      
    }
    
    /**
    * Closing the stage 
    * @param event click on the cross
    */
    @FXML
    void close(MouseEvent event) {
        Cinema.closeStage();
    }

}
