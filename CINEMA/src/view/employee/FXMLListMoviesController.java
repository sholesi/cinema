package view.employee;

import controller.Cinema;

import controller.EmployeeApplication;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class FXMLListMoviesController implements Initializable {

	@FXML
	private Button buttonReturn;

	@FXML
	private Label labelTitle;

	@FXML
	private ListView<String> listMovies;

	@FXML
	private Button buttonSelect;
        
        @FXML
        private ImageView buttonClose;

        /**
         * recovers the movie clicked on the list 
         * 
         * @param event
         * @throws IOException 
         */
	@FXML
	void displayWindowUpdate(ActionEvent event) throws IOException {
		String movieName = listMovies.getSelectionModel().getSelectedItem();

		EmployeeApplication.setLastMovieClicked(movieName);

		Cinema.changeScene("/view/employee/FXMLUpdateMovie.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

        /**
         * recovers the list of all the movies and add them on the listView
         */
	private void loadData() {

		ArrayList<String> moviesTitle = new ArrayList<String>();
		System.out.println(EmployeeApplication.getListMovies().size());
		for (int i = 0; i < EmployeeApplication.getListMovies().size(); i++) {
			moviesTitle.add(EmployeeApplication.getListMovies().get(i).getName());

		}

		listMovies.getItems().addAll(moviesTitle);
	}

        /**
         * 
         * @param event
         * @throws IOException 
         */
	@FXML
	void actionReturn(ActionEvent event) throws IOException {

		Cinema.changeScene("/view/employee/FXMLEmployee.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

        /**
         * load the data at the loading of the window
         * @param location
         * @param resources 
         */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loadData();
	}
        
        /**
        * Closing the stage 
        * @param event click on the cross
        */
        @FXML
        void close(MouseEvent event) {
            Cinema.closeStage();
        }

}
