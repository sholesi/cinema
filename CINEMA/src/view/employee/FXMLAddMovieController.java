package view.employee;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import controller.Cinema;
import controller.EmployeeApplication;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class FXMLAddMovieController implements Initializable{

    @FXML
    private GridPane gridPane;

    @FXML
    private Label labelPoster;

    @FXML
    private Label labelName;

    @FXML
    private Label labelPrice;

    @FXML
    private Label labelDesc;

    @FXML
    private TextField textName;

    @FXML
    private TextField textPrice;

    @FXML
    private TextArea textDesc;

    @FXML
    private Label labelTitle;
    
    @FXML
    private Label labelTime;

    @FXML
    private TextField textTime;
    
    @FXML
    private Button ButtonSelectImage;

    @FXML
    private ImageView imageView;

    @FXML
    private Button buttonSubmit;
    
    @FXML
    private Button buttonReturn;
    
    @FXML
    private DatePicker dateFrom;

    @FXML
    private DatePicker dateTo;
    
    @FXML
    private TextField textDuration;
    
    @FXML
    private CheckBox hour5;

    @FXML
    private CheckBox hour8;

    @FXML
    private CheckBox hour10;    
    
    @FXML
    private ComboBox<Integer> comboRoom;
    
    
    @FXML
    private ImageView buttonClose;
    
    byte[] imageByte ;
    String imageURL;
    

    /**
    * come back to the previous window
    * 
    * @param event click on the button
    */
    @FXML
    void actionReturn(ActionEvent event) throws IOException {
    	 Cinema.changeScene("/view/employee/FXMLEmployee.fxml", Cinema.getPrimaryStage(), true,"employee.css",false);
    }
    
    /**
    * add a new movie
    * 
    * @param event clik on the button
    */
    @FXML
    void addMovie(ActionEvent event) {
  
        String name = textName.getText();
        double price = Double.parseDouble(textPrice.getText());
        String desc = textDesc.getText();
        LocalDate dateBeginning = dateFrom.getValue();
        LocalDate dateFinal = dateTo.getValue();
        String hour = checkHoursSelected();
        int duration = Integer.parseInt(textDuration.getText());
        int room = comboRoom.getSelectionModel().getSelectedItem();
        
        
        String dateB = dateBeginning.toString();
        String dateF = dateFinal.toString();
        
        EmployeeApplication.addMovie(name, price, desc, imageURL, dateB, dateF, hour, duration, room);
                

        Stage newStage = new Stage();
        BorderPane comp = new BorderPane();
        comp.setCenter(new Label("Movie added successfully."));

        Scene stageScene = new Scene(comp, 300, 100);
        newStage.setScene(stageScene);
        newStage.show();

    }
    
    /**
    * upload an image from our folders, to put the image of the movie
    * 
    * @param event clik on the button
    * @exception IOException is produced by failed or interrupted I/O operations.
    */
    @FXML
    void uploadImage(ActionEvent event) throws IOException {
 
        
        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();

        //Set extension filter
        FileChooser.ExtensionFilter extFilterJPG
                = new FileChooser.ExtensionFilter("JPG files (*.JPG)", "*.JPG");
        FileChooser.ExtensionFilter extFilterjpg
                = new FileChooser.ExtensionFilter("jpg files (*.jpg)", "*.jpg");
        FileChooser.ExtensionFilter extFilterPNG
                = new FileChooser.ExtensionFilter("PNG files (*.PNG)", "*.PNG");
        FileChooser.ExtensionFilter extFilterpng
                = new FileChooser.ExtensionFilter("png files (*.png)", "*.png");
        fileChooser.getExtensionFilters()
                .addAll(extFilterJPG, extFilterjpg, extFilterPNG, extFilterpng);
        //Show open file dialog
        
        File file = fileChooser.showOpenDialog(stage);
        File fileRepository=new File("./");
    
		
        imageURL=fileRepository.toURI().relativize(file.toURI()).toString();
        
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            WritableImage image = SwingFXUtils.toFXImage(bufferedImage, null);
            imageView.setImage(image);
            imageView.setFitWidth(50);
            imageView.setFitHeight(50);
            imageView.scaleXProperty();
            imageView.scaleYProperty();
            imageView.setSmooth(true);
            imageView.setCache(true);
            FileInputStream fin = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];

            for (int readNum; (readNum = fin.read(buf)) != -1;) {
                bos.write(buf, 0, readNum);
            }
            
            imageByte = bos.toByteArray();
            fin.close();
        } catch (IOException ex) {
            Logger.getLogger("ss");
        }
        

        
    }

    /**
    * initialize the datePicker to disable the date before the current date
    * 
    * @param location is the link of the image
    * @param resources 
    */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dateFrom.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();

                setDisable(empty || date.compareTo(today) < 0 );
            }
        });
        
        dateTo.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();

                setDisable(empty || date.compareTo(today) < 0 );
            }
        });
        
        ArrayList<Integer> rooms = new ArrayList<Integer>();
        
        rooms = EmployeeApplication.getIdRooms();
        
        for(int i=0; i<rooms.size(); i++){
            int id = rooms.get(i);
            comboRoom.getItems().addAll(id);
        }
    }
    
    /**
    * check which hours are selected for the movie entered
    * 
    */
    public String checkHoursSelected(){
        String s = "";
        
        if(hour5.isSelected()){
            s += hour5.getText() + " ";
        }
        if(hour8.isSelected()){
            s += hour8.getText() + " ";
        }        
        if(hour10.isSelected()){
            s += hour10.getText() + " ";
        }
        
        return s;
    }
    
    /**
    * Closing the stage 
    * @param event click on the cross
    */
    @FXML
    void close(MouseEvent event) {
        Cinema.closeStage();

    }
    

}
