package view.employee;

import controller.Cinema;

import controller.EmployeeApplication;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class FXMLUpdateStateController implements Initializable {

	@FXML
	private Label labelTitle;

	@FXML
	private ListView<String> listView;

	@FXML
	private Button buttonChange;

	@FXML
	private Button buttonReturn;
        
        @FXML
        private ImageView buttonClose;

        /**
        * come back to the previous window
        * 
        * @param event click on the button
        */
	@FXML
	void actionReturn(ActionEvent event) throws IOException {
		Cinema.changeScene("/view/employee/FXMLEmployee.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

        /**
         * recovers the movie selected and changes its status
         * 
         * @param event 
         */
	@FXML
	void setState(ActionEvent event) {

		String[] movie = listView.getSelectionModel().getSelectedItem().split(",");

		EmployeeApplication.changeStatus(movie[0], movie[1]);

		listView.getItems().clear();

		loadData();
	}

        /**
        * load the data at the loading of the window
        * @param location
        * @param resources 
        */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loadData();
	}

        /**
         * recovers the name and the status of all the movies, and add them at the listView
         */
	private void loadData() {
		ArrayList<String> movies = new ArrayList<String>();

		for (int i = 0; i < EmployeeApplication.getListMovies().size(); i++) {
			String m = EmployeeApplication.getListMovies().get(i).getName() + ","
					+ EmployeeApplication.getListMovies().get(i).getStatus();
			movies.add(m);
		}

		listView.getItems().addAll(movies);
	}
        
        /**
        * Closing the stage 
        * @param event click on the cross
        */
        @FXML
        void close(MouseEvent event) {
            Cinema.closeStage();
        }

}
