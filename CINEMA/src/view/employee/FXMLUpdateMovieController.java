package view.employee;

import controller.Cinema;

import controller.EmployeeApplication;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import model.attribut.Movie;

public class FXMLUpdateMovieController implements Initializable {

	@FXML
	private GridPane gridPane;

	@FXML
	private Label labelPoster;

	@FXML
	private Label labelName;

	@FXML
	private Label labelPrice;

	@FXML
	private Label labelDesc;

	@FXML
	private TextField textName;

	@FXML
	private TextField textPrice;

	@FXML
	private TextArea textDesc;

	@FXML
	private Label labelTime;

	@FXML
	private TextField textTime;

	@FXML
	private Button buttonSubmit;

	@FXML
	private Label labelTitle;

	@FXML
	private Button buttonSelectImage;

	@FXML
	private ImageView imageView;

	@FXML
	private Button buttonReturn;
        
        @FXML
        private DatePicker dateFrom;

        @FXML
        private DatePicker dateTo;

        @FXML
        private CheckBox hour5;

        @FXML
        private CheckBox hour8;

        @FXML
        private CheckBox hour10;
        
        @FXML
        private TextField textDuration;
        
        @FXML
        private ImageView buttonClose;

	byte[] person_image;
	String imageURL;

        /**
        * come back to the previous window
        * 
        * @param event click on the button
        */
	@FXML
	void actionReturn(ActionEvent event) throws IOException {

		Cinema.changeScene("/view/employee/FXMLListMovies.fxml", Cinema.getPrimaryStage(), true, "employee.css",false);

	}

        /**
         * recovers the names of the movies and their status and displays them
         */
	public void setInformation() {
                int idMovie = 0;

		Movie movieSelected = EmployeeApplication.getLastMovieClicked();

		this.textName.setText(movieSelected.getName());
		this.textPrice.setText(Double.toString(movieSelected.getPrice()));
		this.textDesc.setText(movieSelected.getDescription());
                
                
                //recup des horaires
                idMovie = EmployeeApplication.getIdMovie(movieSelected.getName());
                String[] hours = EmployeeApplication.getHours(idMovie);
                
                for(int i=0; i<hours.length; i++){
                    if(hours[i].equals("5pm")){
                        hour5.setSelected(true);
                    }
                    if(hours[i].equals("8:30pm")){
                        hour8.setSelected(true);
                    }
                    if(hours[i].equals("10pm")){
                        hour10.setSelected(true);
                    }
                }
                
                

	}

        
        /**
        * load the data at the loading of the window
        * @param location
        * @param resources 
        */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setInformation();
                dateFrom.setDayCellFactory(picker -> new DateCell() {
                    public void updateItem(LocalDate date, boolean empty) {
                        super.updateItem(date, empty);
                        LocalDate today = LocalDate.now();

                        setDisable(empty || date.compareTo(today) < 0 );
                    }
                });

                dateTo.setDayCellFactory(picker -> new DateCell() {
                    public void updateItem(LocalDate date, boolean empty) {
                        super.updateItem(date, empty);
                        LocalDate today = LocalDate.now();

                        setDisable(empty || date.compareTo(today) < 0 );
                    }
                });
	}

        /**
         * update the information of the movie 
         * 
         * @param event 
         */
	@FXML
	void updateInformation(ActionEvent event) {

		String firstName = EmployeeApplication.getLastMovieClicked().getName();

		String name = textName.getText();
		double price = Double.parseDouble(textPrice.getText());
		String desc = textDesc.getText();
                LocalDate dateBeginning = dateFrom.getValue();
                LocalDate dateFinal = dateTo.getValue();
                String hour = checkHoursSelected();
                int duration = Integer.parseInt(textDuration.getText());
        
        
                String dateB = dateBeginning.toString();
                String dateF = dateFinal.toString();

		EmployeeApplication.updateMovie(firstName, name, price, desc, imageURL, dateB, dateF, hour, duration);

		Stage newStage = new Stage();
		BorderPane comp = new BorderPane();
		comp.setCenter(new Label("Movie updated successfully."));

		Scene stageScene = new Scene(comp, 300, 100);
		newStage.setScene(stageScene);
		newStage.show();
	}
        
        
        /**
         * searchs and recovers an image on our folders
         * @param event 
         */
	@FXML
	void uploadImage(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();

		FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.JPG)", "*.JPG");
		FileChooser.ExtensionFilter extFilterjpg = new FileChooser.ExtensionFilter("jpg files (*.jpg)", "*.jpg");
		FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.PNG)", "*.PNG");
		FileChooser.ExtensionFilter extFilterpng = new FileChooser.ExtensionFilter("png files (*.png)", "*.png");
		fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterjpg, extFilterPNG, extFilterpng);

		File file = fileChooser.showOpenDialog(null);
		File fileRepository = new File("./");
		imageURL = fileRepository.toURI().relativize(file.toURI()).toString();

		try {
			BufferedImage bufferedImage = ImageIO.read(file);
			WritableImage image = SwingFXUtils.toFXImage(bufferedImage, null);
			imageView.setImage(image);
			imageView.setFitWidth(50);
			imageView.setFitHeight(50);
			imageView.scaleXProperty();
			imageView.scaleYProperty();
			imageView.setSmooth(true);
			imageView.setCache(true);
			FileInputStream fin = new FileInputStream(file);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];

			for (int readNum; (readNum = fin.read(buf)) != -1;) {
				bos.write(buf, 0, readNum);
			}
			person_image = bos.toByteArray();
			fin.close();
		} catch (IOException ex) {
			Logger.getLogger("ss");
		}
	}
        
        /**
         * check which hours are selected on the checkboxes
         * 
         * @return s a string of the concatenation of the hours selected
         */
        public String checkHoursSelected(){
            String s = "";

            if(hour5.isSelected()){
                s += hour5.getText() + " ";
            }
            if(hour8.isSelected()){
                s += hour8.getText() + " ";
            }        
            if(hour10.isSelected()){
                s += hour10.getText() + " ";
            }

            return s;
        }  
            
        /**
        * Closing the stage 
        * @param event click on the cross
        */
        @FXML
        void close(MouseEvent event) {
            Cinema.closeStage();
        }
        
}
