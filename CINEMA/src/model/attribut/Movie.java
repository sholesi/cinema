package model.attribut;

public class Movie {

	private int id;
	private String name;
	private double price;
	private int duration;
	private boolean status;
	private int ticketPurchase;
	private String description;
	private String url;

	public Movie(int id, String name, int duration,double price, boolean status, int ticketPurchase,
			String description, String url) {
		this.id = id;
		this.name = name;
		this.duration = duration;
		this.price = price;
		this.status = status;
		this.ticketPurchase = ticketPurchase;
		this.description = description;
		this.url = url;
	}

	public Movie(String nameM, double priceM, int duration, String desc, int nbTickets, boolean status, String image) {
		this.name = nameM;
		this.price = priceM;
		this.status = status;
		this.ticketPurchase = nbTickets;
		this.description = desc;
		this.duration = duration;
		this.url = image;

	}

	public Movie(String nameM, double priceM, String desc, int nbTickets, boolean status, String image) {
		this.name = nameM;
		this.price = priceM;
		this.status = status;
		this.ticketPurchase = nbTickets;
		this.description = desc;
		this.url = image;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return this.url;
	}

	public int getTicketPurchase() {
		return this.ticketPurchase;
	}

	public String getDescription() {
		return this.description;
	}
	public String getStatus() {
		String state = "";
		if (this.status) {
			state = "Stopped";
		} else {
			state = "In progress";
		}
		return state;
	}

	public boolean getState() {
		return this.status;
	}

	public double getPrice() {
		return this.price;
	}

	public int getDuration() {
		return this.duration;
	}

	public void setStatus(boolean stat) {
		this.status = stat;

	}


}
