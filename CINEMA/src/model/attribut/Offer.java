package model.attribut;

import model.user.Member;

public class Offer {
	private Member cust;
	private Discount dis;
	
	public Offer(Member cust, Discount dis) {
		this.cust = cust;
		this.dis = dis;
	}
	
	public Member getCust() {
		return cust;
	}
	public void setCust(Member cust) {
		this.cust = cust;
	}
	public Discount getDis() {
		return dis;
	}
	public void setDis(Discount dis) {
		this.dis = dis;
	}
}
