package model.attribut;



import model.ICustomer;

public class Ticket {
	private int id;
	private Calendar calendar;
	private String date;
	private double price;
	private Room room;
	private ICustomer custumer;
	private String url;
	private int nbPlace;

	public Ticket(String date, double price, Room room, ICustomer customer, int nbPlace, Calendar cal) {
		this.date = date;
		this.price = price;
		this.room = room;
		this.custumer = customer;
		this.nbPlace = nbPlace;
		this.calendar = cal;
	}

	public void setId(int id) {
		this.id = id;

	}

	public int getId() {
		return id;

	}

	public String getDate() {
		return date;
	}

	public double getPrice() {
		return price;
	}

	public Movie getMovie() {
		return this.room.getMovie();
	}

	public ICustomer getCustumer() {
		return custumer;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;

	}

	public int getNbPlace() {
		return this.nbPlace;
	}

	public Calendar getCalendar() {
		return this.calendar;
	}

}
