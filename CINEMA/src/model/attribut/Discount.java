package model.attribut;
import model.attribut.TypeC.typeC;

public class Discount {
	private int id;
	private String name;
	private typeC type;
	private double reduction;
	
	public Discount(int id, String name, typeC typeC, double reduction) {
		this.id = id;
		this.name = name;
		this.type = typeC;
		this.reduction = reduction;
	}

	public Discount(String name, typeC type, double red) {
		this.type= type;
		this.name = name;
		this.reduction = red;
	}

	public typeC getType() {
		return type;
	}
	
	@Override
	public String toString() {
		return this.name+":"+this.reduction;
		
	}

	public String getName() {
		return this.name;
	}

	public double getReduction() {
		return reduction;
	}

	public int getId() {
		return this.id;
	}
}
