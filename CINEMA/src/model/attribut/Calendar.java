package model.attribut;

import java.time.LocalDate;



public class Calendar {
	private int idCalendar;
	private LocalDate dateFrom;
	private LocalDate dateTo;
	private String hour;
	private Room room;
	
	
	public Calendar(int idCalendar, Room room, String hour, LocalDate dateFrom, LocalDate dateTo) {
		this.idCalendar = idCalendar;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.hour = hour;
		this.room = room;
	}

	public Room getRoom() {
		return this.room;
	}
	
	public int getIdCalendar() {
		return idCalendar;
	}


	public LocalDate getDateFrom() {
		return dateFrom;
	}


	public LocalDate getDateTo() {
		return dateTo;
	}


	public String getHour() {
		return hour;
	}


	public void setRoom(Room room) {
		this.room = room;
	}


	public Integer getId() {
		return this.idCalendar;
	}

}
