package model.attribut;

public class Room {

	private int id;
	private String name;
	private int seatAvailable;
	private Movie movie;

	public Room(int id, String name, int seat, Movie movie) {
		this.id = id;
		this.name = name;
		this.seatAvailable = seat;
		this.movie = movie;
	}

	public int getID() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public int getSeat() {
		return this.seatAvailable;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
