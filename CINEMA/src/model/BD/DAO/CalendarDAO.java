package model.BD.DAO;

import java.util.ArrayList;

import model.attribut.Calendar;
import model.attribut.Room;

public interface CalendarDAO {
	/**
	 * Getting all the calendar (date range)
	 * @param rooms rooms related
	 * @return the calendar
	 */
	ArrayList<Calendar> getAllCalendar(ArrayList<Room> rooms);

	/**
     * Obtain the hours corresponding to a movie
     * @param idMovie, the id of the movie
     * @return
     */
	String[] getHours(int idMovie);
}
