package model.BD.DAO;

import java.sql.SQLException;
import java.util.ArrayList;

import model.attribut.Movie;

public interface MovieDAO {
	/**
	 * Update the information of the movie in the database
	 * 
	 * @param firstName, the initial name of the movie clicked
	 * @param name,      the updated name of the movie
	 * @param price,     the updated price of the movie
	 * @param desc,      the updated description of the movie
	 * @param imageByte, the updated image of the movie
	 * @param dateFrom,  the updated date of first appareance of the movie
	 * @param dateTo,    the updated date of last appareance of the movie
	 * @param hour,      the updated hour(s) of the movie
	 * @param duration,  the updated duration of the movie
	 */
	void updateMovie(String firstName, String name, double price, String desc, String imageByte, String dateFrom,
			String dateTo, String hour, int duration);

	/**
	 * Add a new movie on the database with the following information
	 * 
	 * @param name,      the name of the movie
	 * @param price,     the price of the movie
	 * @param desc,      the description of the movie
	 * @param imageByte, the image of the movie
	 * @param dateFrom,  the date of the first appareance of the movie
	 * @param dateTo,    the date of the last appareance of the movie
	 * @param hour,      the hour(s) of projection of the movie
	 * @param duration,  the length of the movie
	 * @param room,      the room where is projected the movie
	 * @return a new object Movie
	 */
	Movie addMovie(String name, double price, String desc, String imageByte, String dateFrom, String dateTo,
			String hour, int duration, int room);

	/**
	 * Getting the list of the movies
	 * 
	 * @return movies list
	 */
	ArrayList<Movie> getListMovies();

	/**
	 * Obtain a table of the three most popular movies
	 *
	 * @return a table of movies
	 * @throws SQLException
	 */

	Movie[] getPopularMovies() throws SQLException;

	/**
	 * Change the status of a movie
	 *
	 * @param movieName, the name of the movie
	 * @param status,    the status of the movie
	 */

	void changeStatus(String movieName, String status);

	/**
	 * Obtain the if of the movie related to the name given
	 * 
	 * @param name
	 * @return the id of the movie
	 */
	int getIdMovie(String name);

}
