package model.BD.DAO;

import model.IEmployee;

public interface EmployeeDAO {
        /**
         * Getting the employee with its login and it password
         * @param pass
         * @param mdp
         * @return 
         */
	IEmployee getEmployee(String pass,String mdp);

	  
}
