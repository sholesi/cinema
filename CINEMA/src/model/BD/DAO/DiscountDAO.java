package model.BD.DAO;

import java.util.ArrayList;
import model.attribut.Discount;

public interface DiscountDAO {
	/**
	 * Getting all the discounts
	 * 
	 * @return discounts
	 */
	ArrayList<Discount> getDiscounts();

	/**
	 * Add a new discount in the database with the following information
	 *
	 * @param name
	 * @param type
	 * @param red
	 * @return a discount
	 */

	Discount addDiscount(String name, String type, double red);
}
