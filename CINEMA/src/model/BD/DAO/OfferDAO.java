package model.BD.DAO;

import model.attribut.Offer;

public interface OfferDAO {
	/**
	 * Creating a offer
	 * @param offer offer 
	 */
	void createOffer(Offer offer);
}
