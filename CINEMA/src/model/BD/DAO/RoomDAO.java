package model.BD.DAO;

import java.util.ArrayList;
import model.attribut.Room;

public interface RoomDAO {
	/**
	 * Getting all the rooms
	 * @return rooms 
	 */
	ArrayList<Room> getRooms();

	/**
	 * Updating a the number of seat in a room 
	 * @param nbPlace number of seat
	 * @param room room concerned
	 */
	void updateRoomSeat(int nbPlace, Room room);

	/**
	 * Getting the list of the rooms's id 
	 * @return list of the rooms's id
	 */
	ArrayList<Integer> getIdRooms();
}
