package model.BD.DAO;

import java.sql.SQLException;
import java.util.ArrayList;

import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;

public interface CustomerDAO<ICustomer> {
	/**
	 * Getting the customer 
	 * @param login login of the customer 
	 * @param mdp password of the customer
	 * @return the customer
	 */
	ICustomer getCustomer(String login, String mdp);
	/**
	 * Creating a customer 
	 * @param customer customer
	 */
	void createCustomer(ICustomer customer);

	ObservableList<PieChart.Data> getDataCustomersType() throws SQLException;
	
	/**
	 * Getting the list of the members
	 * @return list members
	 */
	ArrayList<ICustomer> getListMembers();
}
