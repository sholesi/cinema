package model.BD.DAO;

import java.util.List;

import javafx.scene.image.Image;
import model.attribut.Ticket;

public interface TicketDAO {
	/**
	 * Creating a ticket 
	 * @param ticket ticket 
	 * @return ticket
	 */
	Ticket createTicket(Ticket ticket);

	/**
	 * Updating a ticket 
	 * @param ticket ticket
	 */
	void updateImage(Ticket ticket);

	/**
	 * Getting all the images 
	 * @param idCust customer concerned by the images
	 * @return list of the url of the images  
	 */
	List<Image> getImages(int idCust);
}
