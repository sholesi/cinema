package model.BD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionBD {

	private static Connection conn = null;

	static {
		try {

			String url = "jdbc:mysql://localhost:3306/cinema";
			String user = "root";
			String password = "";
			conn = DriverManager.getConnection(url, user, password);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (conn == null) {
					conn.close();
				}
			} catch (SQLException ex) {

				System.out.println(ex.getMessage());
			}
		}

	}

	/**
	 * Getting the connexion of the database
	 * @return connexion database
	 */
	public static Connection getConnexion() {
		return conn;
	}
}
