package model.BD.ImpDAO;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import model.BD.ConnexionBD;
import model.BD.DAO.TicketDAO;
import model.attribut.Ticket;

public class TicketImpDAO implements TicketDAO {
	private static Connection conn = ConnexionBD.getConnexion();



	@Override
	public Ticket createTicket(Ticket ticket) {
		int id = 0;
		try {
			String sql = new StringBuilder(
					"INSERT INTO ticket(idTicket, date, price, idCalendar, idCustomer,url,nbPlace) VALUES(?,?,?,?,?,?,?)")
							.toString();
			PreparedStatement ps;
			ps = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setObject(1, null);
			ps.setObject(2, ticket.getDate());
			ps.setObject(3, ticket.getPrice());
			ps.setObject(4, ticket.getCalendar().getId());
			ps.setObject(5, ticket.getCustumer().getId());
			ps.setObject(6, null);
			ps.setObject(7, ticket.getNbPlace());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			while (rs.next()) {
				id = rs.getInt(1);
			}
			ticket.setId(id);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ticket;

	}

	@Override
	public void updateImage(Ticket ticket) {
		try {
			String sql = new StringBuilder("UPDATE Ticket SET url=? WHERE idTicket=?").toString();
			PreparedStatement ps;
			ps = conn.prepareStatement(sql);
			ps.setObject(1, ticket.getUrl());
			ps.setObject(2, ticket.getId());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	
	
	@Override
	public List<Image> getImages(int idCust) {
		ArrayList<Image> images = new ArrayList<Image>();
		try {
			Statement st = conn.createStatement();
			String sql = "select url from ticket where idCustomer=" + idCust;
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				if (rs.getString(1) != null) {
					Image im = new Image("file:" + rs.getString(1));
					images.add(im);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return images;
	}
}
