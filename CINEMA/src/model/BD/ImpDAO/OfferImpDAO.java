package model.BD.ImpDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.BD.ConnexionBD;
import model.BD.DAO.OfferDAO;
import model.attribut.Offer;

public class OfferImpDAO implements OfferDAO {
	private static Connection conn = ConnexionBD.getConnexion();

	@Override
	public void createOffer(Offer of) {
		try {
			String sql = new StringBuilder("INSERT INTO Offer(idDiscount,idCustomer) VALUES(?,?)").toString();
			PreparedStatement ps;
			ps = conn.prepareStatement(sql);
			ps.setObject(2, of.getCust().getId());
			ps.setObject(1, of.getDis().getId());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
