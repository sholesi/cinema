package model.BD.ImpDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import controller.EmployeeApplication;
import model.BD.ConnexionBD;
import model.BD.DAO.EmployeeDAO;
import model.BD.DAO.MovieDAO;
import model.attribut.Movie;

public class MovieImplDAO implements MovieDAO {
	private static Connection conn = ConnexionBD.getConnexion();

	@Override
	public void updateMovie(String firstName, String name, double price, String desc, String image, String dateB, String dateF, String hour, int duration) {
            int idMovie = 1;
            try {

			conn.createStatement()
					.executeUpdate("UPDATE Movie SET name = '" + name + "', price = " + price + ", duration = " + duration
							+ ", description = '" + desc + "', url = '" + image + "' WHERE name = '" + firstName
							+ "';");
                        
                        //get the idMovie and put it on calendar
                        ResultSet rs = conn.createStatement().executeQuery("SELECT idMovie FROM movie WHERE name = '"+name+"';");
                        while (rs.next()) {
                            idMovie = rs.getInt("idMovie");
                        }
                        conn.createStatement()
					.executeUpdate("UPDATE calendar SET hour = '"+hour+"', dateFrom = '"+dateB+"', dateTo = '"+
                        dateF+"'WHERE idRoom = (Select idRoom From Rooms Where idMovie ="+ idMovie+")");
		} catch (SQLException ex) {
			System.err.println("We cannot add a new movie.");
			Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	@Override
	public Movie addMovie(String name, double price, String desc, String image, String dateB, String dateF, String hour, int duration, int room) {
		Movie movie = null;
                int idMovie = 1;
		try {
			movie = new Movie(name, price,duration, desc, 0,true, image);
			conn.createStatement()
					.executeUpdate("INSERT INTO movie (name, price, status, description, url, duration) VALUES ('" + name
							+ "'," + price + ",1,'" + desc + "','" + image + "',"+duration+");");
                        
                        ResultSet rs = conn.createStatement().executeQuery("SELECT idMovie FROM movie WHERE name = '"+name+"';");
                        while (rs.next()) {
                            idMovie = rs.getInt("idMovie");
                        }
                        conn.createStatement()
                        .executeUpdate("INSERT INTO calendar(idRoom, hour, dateFrom, dateTo) VALUES("+room+",'"+hour+"','"+dateB+"','"+dateF+"');");
                        conn.createStatement().executeUpdate("UPDATE rooms SET idMovie ="+idMovie+" WHERE idRoom ="+room+";");
		
		} catch (SQLException ex) {
			System.err.println("We cannot add a new movie.");
			Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return movie;
	}

	@Override
	public ArrayList<Movie> getListMovies() {

		ArrayList<Movie> movies = new ArrayList<Movie>();
		try {
			ResultSet rs = conn.createStatement().executeQuery("SELECT * FROM movie ORDER BY ticketPurchase DESC;");

			while (rs.next()) {
				String name = rs.getString("name");
				double price = Double.parseDouble(rs.getString("price"));
				String description = rs.getString("description");
				int tickets = rs.getInt("ticketPurchase");
				int status = Integer.parseInt(rs.getString("status"));
				String url = rs.getString("url");
				if (status == 0) {
					
					movies.add(new Movie(name, price, description, tickets, false, url));
				} else {
					movies.add(new Movie(name, price, description, tickets, true, url));
				}
			}
		} catch (SQLException e) {
			Logger.getLogger(EmployeeApplication.class.getName()).log(Level.SEVERE, null, e);
		}
		return movies;
	}

	@Override
	public Movie[] getPopularMovies() throws SQLException {
		Movie[] tabPopMovies = new Movie[3];
		ArrayList<Movie> list = getListMovies();

		tabPopMovies[0] = list.get(0);
		tabPopMovies[1] = list.get(1);
		tabPopMovies[2] = list.get(2);

		return tabPopMovies;
	}

	@Override
	public void changeStatus(String movieName, String status) {
		try {
			if (status.equals("Stopped")) {
				conn.createStatement().executeUpdate("UPDATE Movie SET status = 1 WHERE name = '" + movieName + "';");
			} else {
				conn.createStatement().executeUpdate("UPDATE Movie SET status = 0 WHERE name = '" + movieName + "';");
			}
		} catch (SQLException ex) {
			Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
		}

	}
        
        @Override
        public int getIdMovie(String name){
            int id = 1;
            try {
                ResultSet rs = conn.createStatement().executeQuery("SELECT idMovie FROM movie WHERE name = '"+name+"';");
                
                while(rs.next()){
                    id = rs.getInt("idMovie");
                }
            } catch (SQLException ex) {
                Logger.getLogger(MovieImplDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return id;
        }

}
