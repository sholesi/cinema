package model.BD.ImpDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.BD.ConnexionBD;
import model.BD.DAO.DiscountDAO;
import model.BD.DAO.EmployeeDAO;
import model.attribut.Discount;
import model.attribut.TypeC.typeC;

public class DiscountImpDAO implements DiscountDAO {

	private static Connection conn = ConnexionBD.getConnexion();


	@Override
	public ArrayList<Discount> getDiscounts() {
		ArrayList<Discount> disc = new ArrayList<Discount>();
		try {
			Statement st = conn.createStatement();
			String sql = "select * from discount";
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {

				Discount d = new Discount(rs.getInt(1), rs.getString(2), typeC.valueOf(rs.getString(3)),
						rs.getDouble(4));
				disc.add(d);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return disc;
	}

	@Override
	public Discount addDiscount(String name, String type, double red) {
		  Discount disc = null;
		  try {
			  disc=new Discount(name, typeC.valueOf(type), red);
	           conn.createStatement().executeUpdate("INSERT INTO discount (name, type, reduction) VALUES ('"+name+"','"+type+"',"+red+");");
		  } catch (SQLException ex) {
	            System.err.println("We cannot add a new movie.");
	            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
	        }
		return disc;
	}

}
