package model.BD.ImpDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import model.BD.ConnexionBD;
import model.BD.DAO.CalendarDAO;
import model.attribut.Calendar;
import model.attribut.Room;

public class CalendarImpDAO implements CalendarDAO {
	private static Connection conn = ConnexionBD.getConnexion();

	
    @Override
    public String[] getHours(int idMovie) {
        String[] tabHours;
        String hours = "";
        
		try {
                    ResultSet rs1 = conn.createStatement().executeQuery("select hour from calendar where idRoom = (Select idRoom From Rooms Where idMovie = "+idMovie+");");

                    while (rs1.next()) {
				hours = rs1.getString("hour");
				
			}
			
		} catch (SQLException e) {
			e.getStackTrace();
		}
        tabHours = hours.split(" ");
        return tabHours;
    }
    
	@Override
	public ArrayList<Calendar> getAllCalendar(ArrayList<Room> rooms) {

		ArrayList<Calendar> cal = new ArrayList<Calendar>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
		try {
			Calendar time = null;

			Statement st = conn.createStatement();
			String sql = "select * from calendar";
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {

				for (Room room : rooms) {
					if (room.getID() == rs.getInt(2)) {
						LocalDate dateFrom = LocalDate.parse(rs.getString(4), formatter);
						LocalDate dateTo = LocalDate.parse(rs.getString(5), formatter);
						time = new Calendar(rs.getInt(1), room, rs.getString(3), dateFrom, dateTo);
						cal.add(time);
					}

				}

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return cal;
	}

}
