package model.BD.ImpDAO;

import java.lang.reflect.Field;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;
import model.ICustomer;
import model.BD.ConnexionBD;
import model.BD.DAO.CustomerDAO;
import model.attribut.TypeC.typeC;
import model.user.Member;
import view.employee.FXMLCustomerRecordsController;

public class CustomerImpDAO implements CustomerDAO<ICustomer> {

	private static Connection conn = ConnexionBD.getConnexion();

	@Override
	public ICustomer getCustomer(String login, String mdp) {
		ICustomer cus = null;
		ResultSet rs = null;
		try {
			String sql = "select * from member as m where m.login=? and m.password=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, login);
			ps.setString(2, mdp);
			rs = ps.executeQuery();

			while (rs.next()) {
				cus = new Member(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						typeC.valueOf(rs.getString(6)));
			}
		} catch (SQLException e) {
			e.getStackTrace();
		}
		return cus;
	}

	@Override
	public void createCustomer(ICustomer customer) {
		try {
			Field[] attribute = customer.getClass().getDeclaredFields();
			int id = 0;
			String sql = new StringBuilder("INSERT INTO customer(idCustomer) VALUES(?)").toString();
			PreparedStatement ps = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setObject(1, null);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			while (rs.next()) {
				id = rs.getInt(1);
			}
			customer.setId(id);

			StringBuilder sql_ = new StringBuilder(
					"INSERT INTO " + customer.getClass().getSimpleName() + "(idCustomer");
			if (attribute.length != 0) {
				sql_.append("," + attribute[0].getName());
				for (int i = 1; i < attribute.length; i++) {
					sql_.append("," + attribute[i].getName());
				}
			}
			sql_.append(") VALUES(?");

			if (attribute.length != 0) {
				for (int i = 0; i < attribute.length; i++) {
					sql_.append(",?");
				}
			}

			sql_.append(")");
			sql = sql_.toString();
			ps = conn.prepareStatement(sql);
			ps.setObject(1, id);
			for (int j = 1; j <= attribute.length; j++) {
				attribute[j - 1].setAccessible(true);
				ps.setObject(j + 1, attribute[j - 1].get(customer).toString());
			}
			ps.executeUpdate();

		} catch (SQLException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}

	}

	@Override
	public ObservableList<Data> getDataCustomersType() throws SQLException {
		ObservableList<PieChart.Data> data = null;
		int nbTeenager = 0;
		int nbChild = 0;
		int nbAdult = 0;
		int nbSenior = 0;

		ResultSet rs = conn.createStatement().executeQuery("SELECT type FROM member;");

		while (rs.next()) {
			if (rs.getString("type").equals("Teenager")) {
				nbTeenager++;
			} else if (rs.getString("type").equals("Child")) {
				nbChild++;
			} else if (rs.getString("type").equals("Adult")) {
				nbAdult++;
			} else if (rs.getString("type").equals("Senior")) {
				nbSenior++;
			}
		}

		data = FXCollections.observableArrayList(new PieChart.Data("Child", nbChild),
				new PieChart.Data("Teenager", nbTeenager), new PieChart.Data("Adult", nbAdult),
				new PieChart.Data("Senior", nbSenior));

		return data;
	}


	@Override
	public ArrayList<ICustomer> getListMembers() {
		ResultSet rs;
		ArrayList<ICustomer> members = new ArrayList<ICustomer>();
		try {
			rs = conn.createStatement().executeQuery("SELECT * FROM member;");
			while (rs.next()) {
				String name = rs.getString("firstName");
				String lastName = rs.getString("lastName");
				String login = rs.getString("login");
				String mdp = rs.getString("password");
				String type = rs.getString("type");
				members.add(new Member(name, lastName, login, mdp, typeC.valueOf(type)));

			}
		} catch (SQLException e) {
			Logger.getLogger(FXMLCustomerRecordsController.class.getName()).log(Level.SEVERE, null, e);
		}

		return members;
	}

}
