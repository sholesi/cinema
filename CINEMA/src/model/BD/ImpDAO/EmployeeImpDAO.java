package model.BD.ImpDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.IEmployee;
import model.BD.ConnexionBD;
import model.BD.DAO.EmployeeDAO;
import model.user.Employees;

public class EmployeeImpDAO implements EmployeeDAO {
	private static Connection conn = ConnexionBD.getConnexion();

	@Override
	public IEmployee getEmployee(String login, String mdp) {
		IEmployee em = null;
		ResultSet rs = null;
		try {
			String sql = "select * from employee as e where e.login=? and e.password=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, login);
			ps.setString(2, mdp);
			rs = ps.executeQuery();

			while (rs.next()) {
				em = new Employees(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
			}
		} catch (SQLException e) {
			e.getStackTrace();
		}
		return em;
	}

}
