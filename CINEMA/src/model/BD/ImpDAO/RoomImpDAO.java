package model.BD.ImpDAO;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.BD.ConnexionBD;
import model.BD.DAO.RoomDAO;
import model.attribut.Movie;
import model.attribut.Room;

public class RoomImpDAO implements RoomDAO {

	private static Connection conn = ConnexionBD.getConnexion();

	@Override
	public ArrayList<Room> getRooms() {

		ArrayList<Room> rooms = new ArrayList<Room>();

		try {
			Movie movie = null;
			Statement st = conn.createStatement();
			String sql = "select * from rooms";
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {

				sql = "select * from movie as m where m.idMovie=" + rs.getInt(4);
				Statement st_ = conn.createStatement();
				ResultSet rs_ = st_.executeQuery(sql);
				while (rs_.next()) {
					movie = new Movie(rs_.getInt(1), rs_.getString(2),rs_.getInt(3),rs_.getDouble(4),
							rs_.getBoolean(5), rs_.getInt(6), rs_.getString(7), rs_.getString(8));
					Room r = new Room(rs.getInt(1), rs.getString(2), rs.getInt(3), movie);
					rooms.add(r);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rooms;
	}

	@Override
	public void updateRoomSeat(int nbPlace, Room room) {
		try {
			String sql = new StringBuilder("UPDATE Rooms SET seatAvailable=seatAvailable-? WHERE idRoom=?").toString();
			PreparedStatement ps;
			ps = conn.prepareStatement(sql);
			ps.setObject(1, nbPlace);
			ps.setObject(2, room.getID());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ArrayList<Integer> getIdRooms() {
		ArrayList<Integer> idR = new ArrayList<>();
		try {

			Statement st = conn.createStatement();
			String sql = "select idRoom from rooms where idMovie is null";
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				int room = rs.getInt("idRoom");
				idR.add(room);
			}

		} catch (SQLException ex) {
			Logger.getLogger(RoomImpDAO.class.getName()).log(Level.SEVERE, null, ex);
		}

		return idR;
	}
}
