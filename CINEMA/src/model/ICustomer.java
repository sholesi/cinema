package model;

import java.time.LocalDate;
import java.util.ArrayList;
import model.attribut.Calendar;
import model.attribut.Discount;
import model.attribut.Ticket;
import model.attribut.TypeC.typeC;

public interface ICustomer {
	/**
	 * Buying a movie
	 * @param calendar date range of the movie 
	 * @param date date of the session
	 * @param nbPlace number of place
	 * @param discountList the list of the discounts liked to the customer
	 * @param discount the discount selected
	 * @return a ticket
	 */
	Ticket purchaseMovie(Calendar calendar, String date, int nbPlace, ArrayList<Discount> discountList, Discount discount);
	/**
	 * Setting the id of the customer 
	 * @param id id customer
	 */
	void setId(int id);
	/**
	 * Getting the id of the customer
	 * @return id id customer
	 */
	int getId();
	/**
	 * Getting the type 
	 * @return type
	 */
	typeC getType();
	/**
	 * Calculating the bill for the movie
	 * @param price price of the session
	 * @param nbPlace number of seat
	 * @param discount discounts of the customer 
	 * @param disc discount that the cusomer wants
	 * @return the bill of the purchase
	 */
	double calculateBill(double price, int nbPlace, ArrayList<Discount> discount, Discount disc);
}
