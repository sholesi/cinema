package model.user;

import model.ICustomer;
import model.BD.DAO.RoomDAO;
import model.BD.DAO.TicketDAO;
import model.BD.ImpDAO.RoomImpDAO;
import model.BD.ImpDAO.TicketImpDAO;

public abstract class Customers implements ICustomer {
	protected int idCustomer;
	protected TicketDAO tickDAO = new TicketImpDAO();
	protected RoomDAO roomDAO = new RoomImpDAO();

	@Override
	public int getId() {
		return this.idCustomer;
	}

	@Override
	public void setId(int id) {
		this.idCustomer = id;
	}
}
