package model.user;

import java.util.ArrayList;

import model.BD.DAO.OfferDAO;
import model.BD.ImpDAO.OfferImpDAO;
import model.attribut.Calendar;
import model.attribut.Discount;
import model.attribut.Offer;
import model.attribut.Ticket;
import model.attribut.TypeC.typeC;

public class Member extends Customers {

	protected String firstName;
	protected String lastName;
	private String login;
	private String password;
	private typeC type;

	public Member(int id, String lastName, String firstName, String login, String mdp, typeC typeC) {
		super.idCustomer = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.password = mdp;
		this.type = typeC;
	}

	public Member(String firstN, String lastN, String login, String pass, typeC typeC) {
		this.firstName = firstN;
		this.lastName = lastN;
		this.login = login;
		this.password = pass;
		this.type = typeC;
	}

	@Override
	public Ticket purchaseMovie(Calendar cal,String date, int nbPlace, ArrayList<Discount> discount, Discount disc) {
		OfferDAO offDAO = new OfferImpDAO();
		Ticket ticket = new Ticket(date, this.calculateBill(cal.getRoom().getMovie().getPrice(),nbPlace, discount, disc), cal.getRoom(), this,nbPlace,cal);
		Offer offer = new Offer(this, disc);
		this.roomDAO.updateRoomSeat(nbPlace, cal.getRoom());
		offDAO.createOffer(offer);
		return tickDAO.createTicket(ticket);
	}

	@Override
	public double calculateBill(double price, int nbPlace, ArrayList<Discount> discount, Discount disc) {
		double bill = price;
		for (Discount dis : discount) {
			bill += dis.getReduction();
		}
		bill += disc.getReduction();
		return nbPlace*bill;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public typeC getType() {
		return this.type;
	}

}
