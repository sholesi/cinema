package model.user;

import model.IEmployee;

public class Employees implements IEmployee {
	private int id;
	private String firstName;
	private String lastName;
	private String login;
	private String password;
	

	public Employees(int id, String firstName, String lastName, String login, String password) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.password = password;
	}
	

}
