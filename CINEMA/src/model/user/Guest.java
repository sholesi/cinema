package model.user;

import java.util.ArrayList;

import model.attribut.Calendar;
import model.attribut.Discount;
import model.attribut.Ticket;
import model.attribut.TypeC.typeC;

public class Guest extends Customers {

	@Override
	public Ticket purchaseMovie(Calendar cal, String date, int nbPlace, ArrayList<Discount> discount, Discount disc) {
		Ticket ticket = new Ticket(date, this.calculateBill(cal.getRoom().getMovie().getPrice(),nbPlace, null, null), cal.getRoom(), this,nbPlace,cal);
		this.roomDAO.updateRoomSeat(nbPlace, cal.getRoom());
		return tickDAO.createTicket(ticket);
	}

	@Override
	public double calculateBill(double price, int nbPlace, ArrayList<Discount> discount, Discount disc) {
		return nbPlace*price;
	}

	@Override
	public typeC getType() {
		return null;
	}
}
